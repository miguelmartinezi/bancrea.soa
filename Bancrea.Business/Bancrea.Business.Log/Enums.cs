﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Business.Log
{
    public enum LogSource
    {
        Client,
        Services,
        Login,
        Core
    }

    public enum LogLevel
    {
        Debug,
        Info,        
        Warn,
        Error,
        Fatal
    }

}
