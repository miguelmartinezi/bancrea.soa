IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SaveLog](@Date datetime, @Thread varchar(255), @Level varchar(20), @Source varchar(20), @Message varchar(4000),@Exception varchar(4000))
AS
BEGIN

	INSERT INTO dbo.[Log]([Date], [Thread], [Level], Logger, [Message], Exception)
	 VALUES(@Date, @Thread, @Level, @Source, @Message, @Exception)

END
GO


GRANT EXECUTE ON [dbo].[SaveLog] TO [public]
GO
