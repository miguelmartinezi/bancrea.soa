﻿using Bancrea.Business.Utilities.Exceptions;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Text;


[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Bancrea.Business.Log
{
    public delegate string FormatMessage();
    public class LogHelper
    {
        private object syncRoot = new object();
        private Dictionary<string, ILog> currentLoggers = new Dictionary<string, ILog>();
        private static LogHelper Instance = new LogHelper();

        /// <summary>
        /// GetLog
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <returns>ILog</returns>
        static ILog GetLog(LogSource source)
        {
            ILog log;
            lock (Instance.syncRoot)
            {
                if (Instance.currentLoggers.ContainsKey(source.ToString()))
                {
                    log = Instance.currentLoggers[source.ToString()];
                }
                else
                {
                    log = LogManager.GetLogger(source.ToString());
                    Instance.currentLoggers.Add(source.ToString(), log);
                }
            }

            return log;
        }

        #region Log overload

        public static void Log(LogSource source, LogLevel logLevel, FormatMessage del)
        {
            ILog log = GetLog(source);
            Log(source, del, logLevel);
        }

        /// <summary>
        /// Log
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <param name="del">FormatMessage</param>
        /// <param name="logLevel">LogLevel</param>
        public static void Log(LogSource source, FormatMessage del, LogLevel logLevel)
        {
            ILog log = GetLog(source);
            Instance.InstanceLog(log, del, logLevel);
        }

        /// <summary>
        /// Log
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <param name="message">string</param>
        /// <param name="logLevel">LogLevel</param>
        public static void Log(LogSource source, string message, LogLevel logLevel)
        {
            ILog log = GetLog(source);
            Instance.InstanceLog(log, message, logLevel);
        }

        /// <summary>
        /// Log
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <param name="logLevel">LogLevel</param>
        /// <param name="format">string</param>
        /// <param name="args">params object[]</param>
        public static void Log(LogSource source, LogLevel logLevel, string format, params object[] args)
        {
            Log(source, string.Format(format, args), logLevel);
        }

        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <param name="level">LogLevel</param>
        /// <param name="exception">Exception</param>
        public static void LogException(LogSource source, LogLevel level, Exception exception)
        {
            LogHelper.Log(source, level, ExceptionFormatter.FlattenExceptionMessage(exception));
        }

        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="source">LogSource</param>
        /// <param name="level">LogLevel</param>
        /// <param name="exception">Exception</param>
        /// <param name="message">string</param>
        /// <param name="args">params object[]</param>
        public static void LogException(LogSource source, LogLevel level, Exception exception, string message, params object[] args)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(message, args);
            sb.Append(" : ").Append(ExceptionFormatter.FlattenExceptionMessage(exception));
            LogHelper.Log(source, level, sb.ToString());
        }

        #endregion

        #region InstanceLog overload

        /// <summary>
        /// InstanceLog
        /// </summary>
        /// <param name="log">ILog</param>
        /// <param name="del">FormatMessage</param>
        /// <param name="logLevel">LogLevel</param>
        private void InstanceLog(ILog log, FormatMessage del, LogLevel logLevel)
        {
            if (!log.Logger.Repository.Configured)
            {
                XmlConfigurator.Configure();
            }

            switch (logLevel)
            {
                case LogLevel.Info:
                    if (log.IsErrorEnabled)
                    {
                        log.Error(del());
                    }
                    break;
                case LogLevel.Debug:
                    if (log.IsDebugEnabled)
                    {
                        log.Debug(del());
                    }
                    break;
                case LogLevel.Warn:
                    if (log.IsWarnEnabled)
                    {
                        log.Warn(del());
                    }
                    break;
                case LogLevel.Error:
                    if (log.IsErrorEnabled)
                    {
                        log.Error(del());
                    }
                    break;
                case LogLevel.Fatal:
                    if (log.IsFatalEnabled)
                    {
                        log.Fatal(del());
                    }
                    break;
            }
        }

        /// <summary>
        /// InstanceLog
        /// </summary>
        /// <param name="log">ILog</param>
        /// <param name="message">string</param>
        /// <param name="logLevel">LogLevel</param>
        private void InstanceLog(ILog log, string message, LogLevel logLevel)
        {
            if (!log.Logger.Repository.Configured)
            {
                XmlConfigurator.Configure();
            }

            switch (logLevel)
            {
                case LogLevel.Info:
                    if (log.IsInfoEnabled)
                    {
                        log.Info(message);
                    }
                    break;
                case LogLevel.Debug:
                    if (log.IsDebugEnabled)
                    {
                        log.Debug(message);
                    }
                    break;
                case LogLevel.Warn:
                    if (log.IsWarnEnabled)
                    {
                        log.Warn(message);
                    }
                    break;
                case LogLevel.Error:
                    if (log.IsErrorEnabled)
                    {
                        log.Error(message);
                    }
                    break;
                case LogLevel.Fatal:
                    if (log.IsFatalEnabled)
                    {
                        log.Fatal(message);
                    }
                    break;
            }
        }

        #endregion

    }
}
