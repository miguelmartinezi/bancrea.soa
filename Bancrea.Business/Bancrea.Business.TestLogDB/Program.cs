﻿using Bancrea.Business.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Business.TestLogDB
{
    class Program
    {
        static void Main(string[] args)
        {
            LogHelper.Log(LogSource.Client, LogLevel.Debug, "Prueba escribir bitacora en nivel DEBUG");
            LogHelper.Log(LogSource.Client, LogLevel.Info, "Prueba escribir bitacora en nivel INFO");
            LogHelper.Log(LogSource.Client, LogLevel.Warn, "Prueba escribir bitacora en nivel WARNING");
            //here you may try to use LogException to test it.
            LogHelper.Log(LogSource.Client, LogLevel.Error, "Prueba escribir bitacora en nivel ERROR");
            LogHelper.Log(LogSource.Client, LogLevel.Debug, "Prueba escribir bitacora en nivel FATAL");


            Console.WriteLine("Revisar la bitacora en la Base de datos y los archivos de texto:");
            Console.WriteLine("Base de datos: Bancrea.Log");
            Console.WriteLine(@"C:\Bancrea\UnitTest\Test.txt", "Bancrea.Log");

            Console.ReadLine();
        }
    }
}
