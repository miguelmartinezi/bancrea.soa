﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Business.Utilities.Constants
{
    public class ClientService
    {
        //Super User
        public const string SUPER_USER = "usrauth";
        //Token
        public const string BANCREA_CLIENT_DATA_CONTEXT = "Bancrea.Client";
        public const string TOKEN_PATH = "/token";
        public const int TOKEN_EXPIRES_TIME = 30;

        //LDAP
        public const string BANCREA_AD_HOST = "10.20.1.10";
        public const string BANCREA_AD_DOMAIN = "Bancrea";
        public const string TOKEN_HEADER_ACAO = "Access-Control-Allow-Origin";

        //Parameters
        public const string PARAMETER_CLIENT_ID = "client_id";
        public const string PARAMETER_REFRESH_TOKEN_LIFETIME = "as:clientRefreshTokenLifeTime";
        public const string PARAMETER_CLIENT_ALLOWED_ORIGIN = "as:clientAllowedOrigin";
        public const string PARAMETER_CLIENT_USER_NAME = "userName";

        //Claims
        public const string CLAIM_SUBJECT = "Subject";
        public const string CLAIM_NEW_CLAIM = "newClaim";
        public const string CLAIM_NEW_VALUE = "newValue";


        //Informix
        public const string BANCREA_INFORMIX_BCOLTP_CONNSTRING = 
            "Server=bcoltp_shm;Host=10.19.2.1;Service=19001;Protocol=olsoctcp;UID=informix;PWD=8AnCre4&@;Database={0};";


        //App Settings
        public const string TOKEN_EXPIRES_TIME_APPSETTING = "TokenExpires";
        public const string BANCREA_AD_DOMAIN_APPSETTING = "ADDomain";
        public const string BANCREA_AD_HOST_APPSETTING = "ADHost";
        public const string BANCREA_INFORMIX_BCOLTP_CONNSTRING_APPSETTING = "InformixBcoltpConnString";
    }
}
