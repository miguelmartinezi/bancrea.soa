﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bancrea.Business.Utilities.Exceptions
{
    public static class ExceptionFormatter
    {
        /// <summary>
        /// Flatten the exception
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="ex"></param>
        private static void FlattenException(StringBuilder sb, Exception ex)
        {
            if (sb.Length > 0)
                sb.Append("<-");
            sb.Append("[").Append(ex.GetType().Name).Append("(").Append(ex.Message).Append(")").Append("]");

            if (null != ex.InnerException)
                FlattenException(sb, ex.InnerException);
        }

        /// <summary>
        /// Filter stack trace to only user code.
        /// Basically, user code has file+line information
        /// and microsoft code does not.
        /// </summary>
        /// <param name="stackTrace"></param>
        /// <returns></returns>
        private static string FilterStackTrace(string stackTrace)
        {
            string userStackTrace;
            if (null != stackTrace)
            {
                var lines = stackTrace
                    .Split(new[] { Environment.NewLine }, StringSplitOptions.None)
                    .Where(l => Regex.IsMatch(l, @"([^\)]*\)) in (.*):line (\d)*$"));
                userStackTrace = string.Join(Environment.NewLine, lines);
                // now, we really want the first line of microsoft code so we know the call that caused all this..
                // so, get the substring before the above results
                // and then extract the last portion of it
                var locality = stackTrace.IndexOf(userStackTrace);
                if (locality >= 0 && locality < stackTrace.Length)
                {
                    var subst = stackTrace.Substring(0, locality);
                    var localityProxima = subst.LastIndexOf("at ");
                    if (localityProxima >= 0 && localityProxima < subst.Length)
                    {
                        var candidate = subst.Substring(localityProxima);
                        userStackTrace = candidate + userStackTrace;
                    }
                }
                // don't want multiple lines in log, so replace CR+LF with ' : '
                return userStackTrace.Replace("\r\n", " : ").Replace("\n", " : ").Replace("\r", " : ");
            }
            else
                return "";
        }

        /// <summary>
        /// Flattens the exception message and stack trace.
        /// </summary>
        /// <param name="exception">The exception to flatten.</param>
        /// <returns>A string value with all InnerMessage and StackTrace property text.</returns>
        public static string FlattenExceptionMessage(Exception exception)
        {
            // change format of exception to fit on one line.  Log is extremely messy with indented multi-line format.
            // if user needs to see this, they can replace the separator with CRLF as necessary
            var sb = new StringBuilder();
            //string indent = "";
            FlattenException(sb, exception);

            // include stack trace?
            // only last line after 'at ' we'll need to ensure how this is formatted
            string stack = FilterStackTrace(exception.StackTrace);
            sb.Append(" : user stack trace : ").Append(stack);

            return sb.ToString();
        }
    }
}
