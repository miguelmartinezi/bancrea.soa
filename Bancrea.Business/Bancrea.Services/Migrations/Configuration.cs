namespace Bancrea.Services.Migrations
{
    using Authorization.Models;
    using Business.Utilities.Algorithm;
    using Business.Utilities.Constants;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Configuration : DbMigrationsConfiguration<Bancrea.Services.Authorization.Data.AuthDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Bancrea.Services.Authorization.Data.AuthDataContext context)
        {
            if (context.Clients.Count() > 0)
            {
                return;
            }

            context.Clients.AddRange(AddClients());
            context.AppSettings.AddRange(AddAppSettings());

            context.SaveChanges();
        }

        private static List<Client> AddClients()
        {
            var clients = new List<Client>();

            clients.Add(new Client
            {
                Id = "BancreaESB",
                Secret = HashAlgorithmHelper.GetHash("Bancrea@2017"),
                Name = "Bancrea Enterprise Service Bus",
                ApplicationType = ApplicationTypes.NativeConfidential,
                Active = true,
                RefreshTokenLifeTime = 7200,
                AllowdOrigin = "http://bancrea.esb.com"
            });

            return clients;
        }

        private static List<AppSettings> AddAppSettings()
        {
            var settings = new List<AppSettings>();

            settings.Add(new AppSettings
            {
                Name = ClientService.TOKEN_EXPIRES_TIME_APPSETTING,
                CurrentValue = ClientService.TOKEN_EXPIRES_TIME.ToString(),
                DefaultValue = ClientService.TOKEN_EXPIRES_TIME.ToString(),
                DescriptionKey = "Token Expire time"
            });

            settings.Add(new AppSettings
            {
                Name = ClientService.BANCREA_AD_HOST_APPSETTING,
                CurrentValue = ClientService.BANCREA_AD_HOST,
                DefaultValue = ClientService.BANCREA_AD_HOST,
                DescriptionKey = "Bancrea Active Directory Host"
            });

            settings.Add(new AppSettings
            {
                Name = ClientService.BANCREA_AD_DOMAIN_APPSETTING,
                CurrentValue = ClientService.BANCREA_AD_DOMAIN,
                DefaultValue = ClientService.BANCREA_AD_DOMAIN,
                DescriptionKey = "Bancrea Active Directory Domain"
            });

            settings.Add(new AppSettings
            {
                Name = ClientService.BANCREA_INFORMIX_BCOLTP_CONNSTRING_APPSETTING,
                CurrentValue = ClientService.BANCREA_INFORMIX_BCOLTP_CONNSTRING,
                DefaultValue = ClientService.BANCREA_INFORMIX_BCOLTP_CONNSTRING,
                DescriptionKey = "Informix BCOLTP Connection String"
            });

            return settings;
        }
    }
}
