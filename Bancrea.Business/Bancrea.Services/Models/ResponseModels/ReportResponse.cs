﻿

using Bancrea.Services.Report.Models;
using System.Collections.Generic;

namespace Bancrea.Services.Models.ResponseModels
{
    public class ReportResponse : BaseResponse
    {
        public List<ConstanciaVencInv> ReportList { get; set; }
    }
}
