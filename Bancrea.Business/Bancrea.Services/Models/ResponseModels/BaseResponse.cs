﻿
namespace Bancrea.Services.Models.ResponseModels
{
    public abstract class BaseResponse
    {
        public bool Success { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
    }
}
