﻿using Bancrea.Business.Log;
using Bancrea.Business.Utilities.Constants;
using Bancrea.Services.Authorization.Data;
using Bancrea.Services.Models.ResponseModels;
using Bancrea.Services.Report.Models;
using IBM.Data.Informix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace Bancrea.Services.Report
{
    public class ReportService : IDisposable
    {
        private AuthDataContext context;

        public ReportService()
        {            
            context = new AuthDataContext();

        }

        public ReportResponse GetInvestmentMaturity(string company, DateTime startedDate, DateTime finishedDate)
        {
            ReportResponse response = new ReportResponse();
            IfxConnection connection = null;
            IfxCommand command = null;
            IfxDataReader reader = null;
            List<ConstanciaVencInv> investmentsMaturity = new List<ConstanciaVencInv>();

            var connectionString = context.AppSettings.Where(a => a.Name ==
            ClientService.BANCREA_INFORMIX_BCOLTP_CONNSTRING_APPSETTING).SingleOrDefault().CurrentValue;
            
            connectionString = string.Format(connectionString, "bdinvers");
            try
            {
                connection = new IfxConnection();
                connection.ConnectionString = connectionString;
                connection.Open();

                command = connection.CreateCommand();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "bsv_spconstanciavencinv";
                command.Parameters.Add(new IfxParameter("@pchempresa", IfxType.Char1, 3, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Current, company));
                command.Parameters.Add(new IfxParameter("@pchFechaIni", IfxType.Date, 0, ParameterDirection.Input, false, 0, 0, "",
                    DataRowVersion.Current, startedDate));
                command.Parameters.Add(new IfxParameter("@pchFechaIni", IfxType.Date, 0, ParameterDirection.Input, false, 0, 0, "",
                    DataRowVersion.Current, finishedDate));


                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var investment = new ConstanciaVencInv();

                    investment.CodRet = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    investment.NomEmpresa = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                    investment.FechaHoy = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2);
                    investment.Sucursal = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                    investment.NomSucursal = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                    investment.Plaza = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                    investment.NomPlaza = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
                    investment.Cuenta = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                    investment.Secuencia = reader.IsDBNull(8) ? (int?)null : reader.GetInt16(8);
                    investment.NumCliente = reader.IsDBNull(9) ? string.Empty : reader.GetString(9);
                    investment.NomCliente = reader.IsDBNull(10) ? string.Empty : reader.GetString(10);
                    investment.FechaPerPag = reader.IsDBNull(11) ? (DateTime?)null : reader.GetDateTime(11);
                    investment.InteresPerPag = reader.IsDBNull(12) ? (decimal?)null : reader.GetDecimal(12);
                    investment.CtaCheques = reader.IsDBNull(13) ? string.Empty : reader.GetString(13);
                    investment.FechaAlta = reader.IsDBNull(14) ? (DateTime?)null : reader.GetDateTime(14);
                    investment.FechaVenc = reader.IsDBNull(15) ? (DateTime?)null : reader.GetDateTime(15);
                    investment.Tasa = reader.IsDBNull(16) ? (decimal?)null : reader.GetDecimal(16);
                    investment.Plazo = reader.IsDBNull(17) ? (int?)null : reader.GetInt16(17);
                    investment.Capital = reader.IsDBNull(18) ? (decimal?)null : reader.GetDecimal(18);
                    investment.Interes = reader.IsDBNull(19) ? (decimal?)null : reader.GetDecimal(19);
                    investment.Isr = reader.IsDBNull(20) ? (decimal?)null : reader.GetDecimal(20);
                    investment.Moneda = reader.IsDBNull(21) ? string.Empty : reader.GetString(21);
                    investment.PlazoPerPag = reader.IsDBNull(22) ? (int?)null : reader.GetInt16(22);
                    investment.NomSucursalInv = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);
                    investment.TipoMoneda = reader.IsDBNull(24) ? string.Empty : reader.GetString(24);
                    investment.NomProducto = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                    investment.Producto = reader.IsDBNull(26) ? string.Empty : reader.GetString(26);
                    investment.GpoProducto = reader.IsDBNull(27) ? string.Empty : reader.GetString(27);
                    investment.ProductoSubPro = reader.IsDBNull(28) ? string.Empty : reader.GetString(28);
                    investment.FolioCampana = reader.IsDBNull(29) ? string.Empty : reader.GetString(29);
                    investment.PeriodoCampana = reader.IsDBNull(30) ? (int?)null : reader.GetInt16(30);
                    investmentsMaturity.Add(investment);
                }

                
            }
            catch (Exception ex)
            {
                LogHelper.LogException(LogSource.Services, LogLevel.Error, ex);
                throw ex;
            }
            finally
            {
                connection.Close();
            }

            response.ReportList = investmentsMaturity;
            response.Success = true;
            LogHelper.Log(LogSource.Services, "Peticion exitosa.", LogLevel.Info);

            return response;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}