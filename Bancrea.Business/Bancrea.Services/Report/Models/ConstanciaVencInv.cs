﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Services.Report.Models
{
    public class ConstanciaVencInv
    {
        public string CodRet { get; set; }
        public string NomEmpresa { get; set; }
        public DateTime? FechaHoy { get; set; }
        public string Sucursal { get; set; }
        public string NomSucursal { get; set; }
        public string Plaza { get; set; }
        public string NomPlaza { get; set; }
        public string Cuenta { get; set; }
        public int? Secuencia { get; set; }
        public string NumCliente { get; set; }
        public string NomCliente { get; set; }
        public DateTime? FechaPerPag { get; set; }
        public decimal? InteresPerPag { get; set; }
        public string CtaCheques { get; set; }
        public DateTime? FechaAlta { get; set; }
        public DateTime? FechaVenc { get; set; }
        public decimal? Tasa { get; set; }
        public int? Plazo { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Interes { get; set; }
        public decimal? Isr { get; set; }
        public string Moneda { get; set; }
        public int? PlazoPerPag { get; set; }
        public string NomSucursalInv { get; set; }
        public string TipoMoneda { get; set; }
        public string NomProducto { get; set; }
        public string Producto { get; set; }
        public string GpoProducto { get; set; }
        public string ProductoSubPro { get; set; }
        public string FolioCampana { get; set; }
        public int? PeriodoCampana { get; set; }
    }
}
