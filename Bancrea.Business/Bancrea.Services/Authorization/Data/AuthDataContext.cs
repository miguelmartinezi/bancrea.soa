﻿using Bancrea.Business.Utilities.Constants;
using Bancrea.Services.Authorization.Models;
using System.Data.Entity;

namespace Bancrea.Services.Authorization.Data
{
    public class AuthDataContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshToken { get; set; }

        public DbSet<AppSettings> AppSettings { get; set; }
        public AuthDataContext()
            : base(ClientService.BANCREA_CLIENT_DATA_CONTEXT)
        {

        }

    }
}
