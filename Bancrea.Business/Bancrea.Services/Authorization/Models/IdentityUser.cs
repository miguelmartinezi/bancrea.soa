﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bancrea.Services.Authorization.Models
{
    public class IdentityUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}