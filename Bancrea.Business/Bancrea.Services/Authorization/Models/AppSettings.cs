﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Services.Authorization.Models
{
    public class AppSettings
    {
        [Key]
        public string Name { get; set; }

        public string DescriptionKey { get; set; }

        public string CurrentValue { get; set; }

        public string DefaultValue { get; set; }

    }
}
