﻿using Bancrea.Business.Log;
using Bancrea.Business.Utilities.Constants;
using Bancrea.Services.Authorization.Data;
using Bancrea.Services.Authorization.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;

namespace Bancrea.Services.Authorization
{
    public class AuthService : IDisposable
    {
        private AuthDataContext context;

        public AuthService()
        {            
            context = new AuthDataContext();
            //here initialize anything related with connection to AD

        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            if (IsValidUser(userName, password))
            {
                return new IdentityUser() { UserName = userName, Password = password };
            }
            
            return null;
        }
        private bool IsValidUser(string userName, string password)
        {
            var host = context.AppSettings.Where(a => a.Name ==
            ClientService.BANCREA_AD_HOST_APPSETTING).SingleOrDefault().CurrentValue;

            var domain = context.AppSettings.Where(a => a.Name ==
            ClientService.BANCREA_AD_DOMAIN_APPSETTING).SingleOrDefault().CurrentValue;

            var isAuthenticated = false;
            try
            {
                LogHelper.Log(LogSource.Login, string.Format("Intentando validar usuario: {0}",userName), LogLevel.Info);
                using (PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, host))
                {
                    var userPrincipalName = userName + "@" + domain;
                    isAuthenticated = principalContext.ValidateCredentials(userPrincipalName, password);
                }

                LogHelper.Log(LogSource.Login, 
                    string.Format("usuario {0} {1}", userName, 
                    isAuthenticated ? "tiene acceso" : "no tiene acceso"), LogLevel.Info);
            }
            catch (Exception ex)
            {
                LogHelper.Log(LogSource.Login, string.Format("Usuario invalido: {0}", userName), LogLevel.Info);
                LogHelper.LogException(LogSource.Login, LogLevel.Error, ex);
                throw ex;
            }

            return isAuthenticated;
        }

        public Client FindClient(string clientId)
        {
            var client = context.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken = context.RefreshToken.Where(r => r.Subject == token.Subject && 
            r.Id == token.Id).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            context.RefreshToken.Add(token);

            return await context.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await context.RefreshToken.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                context.RefreshToken.Remove(refreshToken);
                return await context.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            context.RefreshToken.Remove(refreshToken);
            return await context.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await context.RefreshToken.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return context.RefreshToken.ToList();
        }
        public void Dispose()
        {
            context.Dispose();
            //here dispose anything used by AD
        }
    }
}