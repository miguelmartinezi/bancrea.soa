﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

[assembly: log4net.Config.XmlConfigurator(Watch =true)]

namespace Bancrea.Business.Log.Tests
{
    [TestClass]
    public class LogTest
    {
        //private static readonly log4net.ILog log = LogHelper.GetLogger();

        [TestMethod]
        public void LogDebug()
        {
            LogHelper.Log(LogSource.Client, LogLevel.Debug, "This is my test for Log Debug");
        }


        [TestMethod]
        public void LogInfo()
        {
            LogHelper.Log(LogSource.Client, LogLevel.Info, "This is my test for Log Info");
        }

        [TestMethod]
        public void LogWarning()
        {
            LogHelper.Log(LogSource.Client, LogLevel.Warn, "This is my test for Log Warn");
        }


        [TestMethod]
        public void LogError()
        {
            try
            {
                var i = 0;

                var div = 20 / i;
            }
            catch (DivideByZeroException ex)
            {
                LogHelper.LogException(LogSource.Services, LogLevel.Error, ex);
            }
        }

        [TestMethod]
        public void LogFatal()
        {
            LogHelper.Log(LogSource.Login, LogLevel.Fatal, "This is my test for Log Fatal");
        }



    }
}
