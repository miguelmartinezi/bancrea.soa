﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Bancrea.Client.Services.Authorization;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using System.Data.Entity;
using Bancrea.Business.Utilities.Constants;
using System.Web.Routing;

[assembly: OwinStartup(typeof(Bancrea.Client.Services.Startup))]

namespace Bancrea.Client.Services
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            ConfigureOAuth(app);
            
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);
            Database.SetInitializer(new 
                MigrateDatabaseToLatestVersion<Bancrea.Services.Authorization.Data.AuthDataContext, 
                 Bancrea.Services.Migrations.Configuration>());
            
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            //enable cors origin request
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var authorizationServerProvider = new AuthorizationServerProvider();
            var authenticationRefreshTokenProvider = new RefreshTokenProvider();

            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true, //may be changed
                TokenEndpointPath = new PathString(ClientService.TOKEN_PATH),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(ClientService.TOKEN_EXPIRES_TIME), //Review here how get configurable this value
                Provider = authorizationServerProvider,
                RefreshTokenProvider = authenticationRefreshTokenProvider
            };

            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
