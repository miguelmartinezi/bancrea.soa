﻿using Bancrea.Business.Utilities.Constants;
using Bancrea.Services.Authorization;
using System.Threading.Tasks;
using System.Web.Http;

namespace Bancrea.Client.Services.Controllers
{
    public class RefreshTokenController : ApiController
    {
        private AuthService service = null;

        public RefreshTokenController()
        {
            service = new AuthService();
        }

        [Authorize(Users = ClientService.SUPER_USER)]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(service.GetAllRefreshTokens());
        }

        [Authorize(Users = ClientService.SUPER_USER)]
        [Route("")]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            var result = await service.RemoveRefreshToken(tokenId);

            if (result)
            {
                return Ok();
            }

            return BadRequest(Resources.Localized.AUTHORIZATION_TOKEN_ID_DOES_NOT_EXIST);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
