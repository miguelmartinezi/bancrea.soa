﻿
using System.Web.Http;
using System;
using Bancrea.Services.Models.ResponseModels;
using Bancrea.Services.Report;
using Bancrea.Business.Log;

namespace Bancrea.Client.Services.Controllers
{
    [RoutePrefix("api/report")]
    public class ReportController : ApiController
    {
        ReportService reportService;

        public ReportController()
        {
            reportService = new ReportService();
        }

        [Authorize]
        [Route("")]
        public ReportResponse Get(string company, DateTime startedDate, DateTime finishedDate)
        {
            LogHelper.Log(LogSource.Services, "Accediendo al reporte de inversiones", LogLevel.Info);
            return reportService.GetInvestmentMaturity(company, startedDate, finishedDate);
        }

        //[Authorize]
        //public IHttpActionResult Get(string company, DateTime startedDate, DateTime finishedDate)
        //{
        //    return Ok("done");
        //}

        protected override void Dispose(bool disposing)
        {
            reportService.Dispose();
            base.Dispose(disposing);
        }
    }
}