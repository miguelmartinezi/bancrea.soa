﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Bancrea.Client.Services.Controllers
{
    [RoutePrefix("api/data")]
    public class DataController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("")]    
        public IHttpActionResult Get()
        {
            return Ok("Now server time is:" + DateTime.Now.ToString());
        }

        [Authorize]
        [HttpGet]
        [Route("authenticate")]
        public IHttpActionResult GetForAuthenticate()
        {
            var identity = (ClaimsIdentity)User.Identity;

            return Ok("Hello " + identity.Name);
        }

        [Authorize(Users = "usrauth")]
        [HttpGet]
        [Route("authorize")]
        public IHttpActionResult GetForAdmin()
        {
            var identity = (ClaimsIdentity)User.Identity;

            return Ok("Hello " + identity.Name);
        }
    }
}
