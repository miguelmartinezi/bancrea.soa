﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Bancrea.Services.Authorization;
using Bancrea.Services.Authorization.Models;
using Bancrea.Business.Utilities.Algorithm;
using Bancrea.Business.Utilities.Constants;

namespace Bancrea.Client.Services.Authorization
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientId = context.Ticket.Properties.Dictionary[ClientService.PARAMETER_CLIENT_ID];

            if (string.IsNullOrEmpty(clientId))
            {
                return;
            }

            //Generate a unique string generation algorithm for next release.
            var refreshTokenId = Guid.NewGuid().ToString("n");

            using (var authService = new AuthService())
            {
                var refreshTokenLifeTime = 
                    context.OwinContext.Get<string>(ClientService.PARAMETER_REFRESH_TOKEN_LIFETIME);

                var token = new RefreshToken()
                {
                    Id = HashAlgorithmHelper.GetHash(refreshTokenId),
                    ClientId = clientId,
                    Subject = context.Ticket.Identity.Name,
                    IssuedUtc = DateTime.UtcNow,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

                token.ProtectedTicket = context.SerializeTicket();

                var result = await authService.AddRefreshToken(token);

                if (result)
                {
                    context.SetToken(refreshTokenId);
                }
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>(ClientService.PARAMETER_CLIENT_ALLOWED_ORIGIN);
            context.OwinContext.Response.Headers.Add(ClientService.TOKEN_HEADER_ACAO, new[] { allowedOrigin });

            var hashedTokenId = HashAlgorithmHelper.GetHash(context.Token);

            using (var authService = new AuthService())
            {
                var refreshToken = await authService.FindRefreshToken(hashedTokenId);

                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    var result = await authService.RemoveRefreshToken(hashedTokenId);
                }
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

    }
}