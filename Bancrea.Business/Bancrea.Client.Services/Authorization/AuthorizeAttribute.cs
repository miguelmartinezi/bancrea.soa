﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace Bancrea.Client.Services.Authorization
{
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                //Forbiden returns 403
                actionContext.Response = new
                    System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
            
        }
    }
}