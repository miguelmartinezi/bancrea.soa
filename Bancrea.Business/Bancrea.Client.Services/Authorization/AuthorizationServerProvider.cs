﻿using Bancrea.Business.Utilities.Algorithm;
using Bancrea.Business.Utilities.Constants;
using Bancrea.Services.Authorization;
using Bancrea.Services.Authorization.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.Client.Services.Authorization
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            //context.Validated();   
            var clientId = string.Empty;
            var clientSecret = string.Empty;
            Bancrea.Services.Authorization.Models.Client client = null;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }



            if (context.ClientId == null)
            {
                //Remove the comments from the below line context.SetError, and invalidate context 
                //if you want to force sending clientId/secrects once obtain access tokens. 
                context.Validated();
                //context.SetError("invalid_clientId", "ClientId should be sent.");
                return Task.FromResult<object>(null);
            }

            using (var service = new AuthService())
            {
                client = service.FindClient(context.ClientId);
            }

            if (client == null)
            {
                context.SetError(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_ERROR, 
                    string.Format(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_UNREGISTERED,
                    context.ClientId));

                return Task.FromResult<object>(null);
            }

            if (client.ApplicationType == ApplicationTypes.NativeConfidential)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_ERROR, 
                        Resources.Localized.AUTHORIZATION_INVALID_CLIENT_NO_SECRET);
                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (client.Secret != HashAlgorithmHelper.GetHash(clientSecret))
                    {
                        context.SetError(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_ERROR, 
                            Resources.Localized.AUTHORIZATION_INVALID_CLIENT_SECRET_INVALID);
                        return Task.FromResult<object>(null);
                    }
                }
            }

            if (!client.Active)
            {
                context.SetError(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_ERROR, 
                    Resources.Localized.AUTHORIZATION_INVALID_CLIENT_INACTIVE);
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set<string>(ClientService.PARAMETER_CLIENT_ALLOWED_ORIGIN, client.AllowdOrigin);
            context.OwinContext.Set<string>(ClientService.PARAMETER_REFRESH_TOKEN_LIFETIME, client.RefreshTokenLifeTime.ToString());

            context.Validated();

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>(ClientService.PARAMETER_CLIENT_ALLOWED_ORIGIN);

            if (allowedOrigin == null)
            {
                allowedOrigin = "*";
            }

            context.OwinContext.Response.Headers.Add(ClientService.TOKEN_HEADER_ACAO, new[] { allowedOrigin });

            try
            {
                using (var authService = new AuthService())
                {
                    IdentityUser user = await authService.FindUser(context.UserName, context.Password);

                    if (user == null)
                    {
                        context.SetError(Resources.Localized.AUTHORIZATION_INVALID_GRANT_ERROR,
                            Resources.Localized.AUTHORIZATION_INVALID_GRANT_INVALID_USER);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                context.SetError(ex.Message, ex.StackTrace);
                return;
            }


            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //here add all claims
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim(ClientService.CLAIM_SUBJECT, context.UserName));

            var properties = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    ClientService.PARAMETER_CLIENT_ID, (context.ClientId == null) ? string.Empty : context.ClientId
                },
                {
                    ClientService.PARAMETER_CLIENT_USER_NAME, context.UserName
                }
            });

            //add roles
            identity.AddClaim(new Claim(ClaimTypes.Role, "regular"));

            var ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string,string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary[ClientService.PARAMETER_CLIENT_ID];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError(Resources.Localized.AUTHORIZATION_INVALID_CLIENT_ERROR,
                    Resources.Localized.AUTHORIZATION_INVALID_CLIENT_REFRESH_TOKEN_ISSUED_TO_DIFFERENT);
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim(ClientService.CLAIM_NEW_CLAIM, ClientService.CLAIM_NEW_VALUE));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }
    }
}
