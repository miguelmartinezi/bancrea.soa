-- NOTE:  this script is called via the installer directly if the HMI is being installed.  
IF NOT EXISTS(SELECT * FROM master.dbo.sysdatabases WHERE (name  = N'Bancrea.Log'))
begin
	CREATE DATABASE [Bancrea.Log]
end
GO

USE [Bancrea.Log]
IF EXISTS(select * from dbo.sysobjects where id = object_id(N'[dbo].[Log]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Log]
GO

CREATE TABLE [dbo].[Log] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [varchar] (4000) NOT NULL,
    [Exception] [varchar] (2000) NULL
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

setuser
GO


USE [Bancrea.Log]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SaveLog](@Date datetime, @Thread varchar(255), @Level varchar(20), @Source varchar(20), @Message varchar(4000),@Exception varchar(4000))
AS
BEGIN

	INSERT INTO dbo.[Log]([Date], [Thread], [Level], Logger, [Message], Exception)
	 VALUES(@Date, @Thread, @Level, @Source, @Message, @Exception)

END
GO


GRANT EXECUTE ON [dbo].[SaveLog] TO [public]
GO
