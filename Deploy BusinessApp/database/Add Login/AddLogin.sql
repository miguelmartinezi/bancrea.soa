declare @NtUserId nchar(50) = N'Bancrea Web User'

declare @MachineName nvarchar(255);
select @MachineName=CAST(SERVERPROPERTY('MachineName') as nvarchar(255))

declare @NewNtUserId nvarchar(256)
select @NewNtUserId = @MachineName + '\' + @NtUserId


if not exists(select loginname from master.dbo.syslogins where name = @NewNtUserId and dbname = 'master')
begin
   print 'Login no existe, creandolo...'
   declare @SqlCode nchar(512);

   set @SqlCode = 
     'CREATE LOGIN [' + RTRIM(@NewNtUserId) + '] '
	 + 'FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english] '
	 + 'ALTER SERVER ROLE [sysadmin] ADD MEMBER [' + RTRIM(@NewNtUserId) + '] '

   print @SqlCode

   exec (@SqlCode)
end

GO