using System;
using System.Web.Http;
using System.Web.Mvc;
using Bancrea.EWS.Areas.HelpPage.ModelDescriptions;
using Bancrea.EWS.Areas.HelpPage.Models;
using System.Linq;

namespace Bancrea.EWS.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        private const string ErrorViewName = "Error";

        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult Index()
        {
            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
            //System.Collections.ObjectModel.Collection<System.Web.Http.Description.ApiDescription>
            //    descriptors;

            //descriptors = new System.Collections.ObjectModel.Collection<System.Web.Http.Description.ApiDescription>(
            //    Configuration.Services.GetApiExplorer().ApiDescriptions.Where(a => a.RelativePath.Contains("Test")).ToList()
            //    );

            //return View(descriptors);


            return View(Configuration.Services.GetApiExplorer().ApiDescriptions);

        }

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View(ErrorViewName);
        }

        public ActionResult ResourceModel(string modelName)
        {
            if (!String.IsNullOrEmpty(modelName))
            {
                ModelDescriptionGenerator modelDescriptionGenerator = Configuration.GetModelDescriptionGenerator();
                ModelDescription modelDescription;
                if (modelDescriptionGenerator.GeneratedModels.TryGetValue(modelName, out modelDescription))
                {
                    return View(modelDescription);
                }
            }

            return View(ErrorViewName);
        }
    }
}