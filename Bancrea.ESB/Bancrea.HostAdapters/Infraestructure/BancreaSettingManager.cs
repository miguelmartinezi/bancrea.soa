﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bancrea.ESB.Core.Domain.Global.Model;
using Bancrea.HostAdapters.Infraestructure.Utilities;
using Bancrea.ESB.Core.Infrastructure;

namespace Bancrea.HostAdapters.Infraestructure
{
    public class BancreaSettingManager: HostSettingManager
    {
        protected override IList<HostSetting> HostSettings
        {
            get
            {
                return BancreaSettings; 
            }
        }
        /// <summary>
        /// The iVP settings.
        /// </summary>
        private static IList<HostSetting> BancreaSettings
        {
            get
            {
                return new List<HostSetting> {
                    new HostSetting
                    {
                        Name = BancreaConstants.BancreaAuthApiUrl,
                        DescriptionKey = BancreaConstants.BancreaAuthApiKey,
                        DefaultValue = "http://localhost:57982/token",
                        CurrentValue = "http://localhost:57982/token",
                        Type = HostSettingType.Basic,
                    },
                    new HostSetting
                    {
                        Name = BancreaConstants.BancreaClientApiUrl,
                        DescriptionKey = BancreaConstants.BancreaClientApiKey,
                        DefaultValue = "http://localhost:57982/api/",
                        CurrentValue = "http://localhost:57982/api/",
                        Type = HostSettingType.Basic,
                    }
                };
            }
        }


        /// <summary>
        /// Helper method to build a description key based on the name of the setting.
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        private static string BuildDescriptionKey(string settingName)
        {
            return settingName + "Desc";
        }

        /// <summary>
        /// Returns the locale-sensitive labels/messages.
        /// </summary>
        /// <param name="key">The message key</param>
        /// <returns>Localized messages</returns>
        protected override string GetMessage(string key)
        {
            return BancreaAdapterMessageProvider.GetMessage(key);
        }

    }
}
