﻿using Bancrea.ESB.Core.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infraestructure.Exceptions
{
    public class ParseException : ServiceException
    {
        public ParseException(string msg, Exception innerException)
            : base(msg, innerException)
        {
            // Left empty on purpose.
        }
    }
}
