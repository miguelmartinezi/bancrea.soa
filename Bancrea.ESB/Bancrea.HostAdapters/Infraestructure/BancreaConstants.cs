﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infraestructure
{
    public class BancreaConstants
    {
        #region Setting Keys

        public const string BancreaAuthApiUrl = "BancreaAuthApiUrl";
        public const string BancreaAuthApiKey = "BancreaAuthApiKey";
        public const string BancreaClientApiUrl = "BancreaClientApiUrl";
        public const string BancreaClientApiKey = "BancreaClientApiKey";
        public const string BancreaSSLCertHash = "BancreaSSLCertHash";

        #endregion
    }
}
