﻿using Bancrea.ESB.Core.Infrastructure.Exceptions;
using Bancrea.HostAdapters.Models.Requests;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using StructureMap;
using Bancrea.HostAdapters.Infraestructure.Utilities;
using Newtonsoft.Json.Linq;

namespace Bancrea.HostAdapters.Infraestructure
{
    public class RequestDispatcher : IRequestDispatcher
    {

        public bool IsAuthRequest { get; set; }
        public string Token { get; set; }

        public RequestDispatcher()
        {
        }

        public RequestDispatcher(bool isAuthRequest)
        {
            IsAuthRequest = isAuthRequest;
        }

        public RequestDispatcher(string token)
        {
            Token = token;
        }


        public dynamic Execute(BancreaRequest request)
        {
            dynamic response = Execute(request.RestRequest);

            return request.ParseResponse(response);
        }

        public dynamic Execute(string resource, Method method, bool isAuthRequest, BancreaParameterBag parameters = null)
        {
            return Execute(RestRequestBuilder.BuildRequest(resource, method, isAuthRequest, parameters));
        }

        private void ValidateResponseStatus(IRestResponse response)
        {
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                var serviceFault = new ServiceFault();
                serviceFault.Message = BancreaAdapterMessageProvider.GetMessage("BancreaConnectionErrorMsg");
                serviceFault.Details = response.ErrorMessage;
                throw new ServiceException(serviceFault);
            }
        }

        private void ValidateHttpStatus(IRestResponse response)
        {
            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.NoContent)
            {
                ServiceFault fault = new ServiceFault();

                try
                {
                    dynamic content = JsonConvert.DeserializeObject<dynamic>(response.Content);

                    // Message returned by Bancrea web Services
                    fault.Message = content.StatusMessage == null ? "" : content.StatusMessage.Value;
                    fault.Details = content.Details == null ? "" : content.Details.Value;

                    // Error Message returned by the .NET framework of the Bancrea Web Services. e.g. url doesn't exist
                    // TODO: Should probably make the property name the same the Bancrea Web Services
                    if (string.IsNullOrEmpty(fault.Message))
                    {
                        fault.Message = content.Message == null ? "" : content.Message.Value;
                    }
                    if (string.IsNullOrEmpty((string)fault.Details))
                    {
                        fault.Details = content.MessageDetail == null ? "" : content.MessageDetail.Value;
                    }
                }
                catch (JsonReaderException e)
                {
                    // No valid json content. Bancrea web services probably crashed.
                    fault.Message = BancreaAdapterMessageProvider.GetMessage("BancreaUnknownErrorMsg");
                    fault.Details = BancreaAdapterMessageProvider.GetMessage("BancreaMissingErrorDetailMsg") + (int)response.StatusCode + " " + response.StatusDescription;
                }
                catch (RuntimeBinderException e)
                {
                    // Message or details doesn't exist in the response body.
                    fault.Message = BancreaAdapterMessageProvider.GetMessage("BancreaUnknownErrorMsg");
                    fault.Details = BancreaAdapterMessageProvider.GetMessage("BancreaMissingErrorDetailMsg") + (int)response.StatusCode + " " + response.StatusDescription;
                }

                throw new ServiceErrorException(fault);
            }
        }


        public dynamic Execute(IRestRequest request, string userName = null, string password = null)
        {
            var url = new Uri(BancreaSettingManager.GetSetting(IsAuthRequest ? BancreaConstants.BancreaAuthApiUrl : 
                BancreaConstants.BancreaClientApiUrl));

            IRestClient client = new RestClient { BaseUrl = url };

            //When we decide use culture
            //client.AddDefaultHeader("Accept-Language", Thread.CurrentThread.CurrentUICulture.Name);
            if (IsAuthRequest)
            {
                client.AddDefaultHeader("content-type", "application/x-www-form-urlencoded");
            }
            else
            {
                client.AddDefaultHeader("Authorization", Token);
            }

            //review if you want to decide to writte here in log
            // ???? method ??? LogBancreaRequest(request);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            IRestResponse response = client.Execute(request);
            stopWatch.Stop();

            ValidateResponseStatus(response);

            //review if you want to decide to writte here in log too
            // Validate the response status before logging the Bancrea service response. 
            // This ensures that the response exists.
            // ???? method ??? LogBancreaResponse(response);
            // ???? method ??? LogBancreaService(response, stopWatch.Elapsed);

            ValidateHttpStatus(response);

            return JsonConvert.DeserializeObject<dynamic>(response.Content);
        }

    }

    public class BancreaParameterBag
    {
        public List<BancreaParameter> Parameters { get; set; }

        public bool HasRequestBodyParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.RequestBody).Count() > 0;
            }
        }

        public Dictionary<string, dynamic> RequestBodyParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.RequestBody).ToDictionary(d => d.Key, d => d.Value);
            }
        }

        public string RequestBodyParametersForAuth
        {
            get
            {
                return string.Format("{0}",
                string.Join("&",
                    RequestBodyParameters.Select(rbp =>
                        string.Format("{0}={1}", rbp.Key, rbp.Value))));
            }
        }

        public bool HasQueryStringParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.QueryString).Count() > 0;
            }
        }

        public List<BancreaParameter> QueryStringParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.QueryString).ToList();
            }
        }

        public bool HasUrlSegmentParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.UrlSegment).Count() > 0;
            }
        }

        public List<BancreaParameter> UrlSegmentParameters
        {
            get
            {
                return Parameters.Where(p => p.ParamType == BancreaParameterType.UrlSegment).ToList();
            }
        }

        public BancreaParameterBag(params BancreaParameter[] _parameters)
        {
            Parameters = _parameters.ToList();
        }

        public void SetRequestBody(dynamic body)
        {
            foreach (PropertyInfo property in body.GetType().GetProperties())
            {
                Parameters.Add(new BancreaParameter(property.Name, property.GetValue(body), BancreaParameterType.RequestBody));
            }
        }
    }

    public class BancreaParameter
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }
        public BancreaParameterType ParamType { get; set; }

        public BancreaParameter(string key, dynamic value, BancreaParameterType type)
        {
            Key = key;
            Value = value;
            ParamType = type;
        }
    }

    public enum BancreaParameterType
    {
        RequestBody = 0,
        QueryString = 1,
        UrlSegment = 2
    }

}
