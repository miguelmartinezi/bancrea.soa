﻿using Bancrea.HostAdapters.Models.Requests;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infraestructure
{
    public class RestRequestBuilder
    {
        public static IRestRequest BuildRequest(BancreaRequest request)
        {
            return BuildRequest(request.END_POINT, request.HTTP_METHOD, request.IsAuthRequest, request.ParameterBag);
        }

        public static IRestRequest BuildRequest(string resource, Method method, bool isAuthRequest, BancreaParameterBag parameters = null)
        {
            IRestRequest request = new RestRequest(resource, method); //{ RequestFormat = DataFormat. };

            //var apiKey = Settings.Default.BancreaApiKey;
            //var apiKeyBytes = System.Text.Encoding.UTF8.GetBytes(apiKey);
            //request.AddHeader("ApiKey", System.Convert.ToBase64String(apiKeyBytes));

            if (parameters != null)
            {
                // Should we Implement this in object oriented manner? 
                if (parameters.HasRequestBodyParameters && isAuthRequest)
                {
                    request.AddParameter("application/x-www-form-urlencoded", parameters.RequestBodyParametersForAuth, ParameterType.RequestBody);
                }
                else 
                {
                    if (parameters.HasRequestBodyParameters)
                    {
                        request.AddParameter("text/json", JsonConvert.SerializeObject(parameters.RequestBodyParameters), ParameterType.RequestBody);
                    }
                    
                }

                if (parameters.HasQueryStringParameters)
                {
                    // TODO: FIXME: Restsharp not taking QueryStringParameter properly for PUT request.
                    parameters.QueryStringParameters.ForEach(x => request.AddParameter(x.Key, x.Value));
                }
                if (parameters.HasUrlSegmentParameters)
                {
                    parameters.UrlSegmentParameters.ForEach(x => request.AddParameter(x.Key, x.Value, ParameterType.UrlSegment));
                }

            }

            return request;
        }
    }
}
