﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infraestructure
{
    public interface IRequestDispatcher
    {
        dynamic Execute(string resource, Method method, bool isAuthRequest, BancreaParameterBag parameters = null);
        dynamic Execute(IRestRequest request, string userName = null, string password = null);
    }
}
