﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infraestructure.Utilities
{
    public class BancreaAdapterMessageProvider
    {
        public static string GetMessage(string key)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Bancrea.HostAdapters.Infraestructure.Utilities.BancreaAdapterMessages",
                            System.Reflection.Assembly.GetExecutingAssembly());

            return rm.GetString(key);
        }

        public static string GetMessage(string key, string locale)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Bancrea.HostAdapters.Infraestructure.Utilities.BancreaAdapterMessages",
                System.Reflection.Assembly.GetExecutingAssembly());
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(locale);

            return rm.GetString(key, ci);
        }
    }
}
