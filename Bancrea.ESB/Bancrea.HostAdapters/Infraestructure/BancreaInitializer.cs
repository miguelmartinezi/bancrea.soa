﻿using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.HostAdapters.Infraestructure;
using RestSharp;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Infrastructure
{
    public class BancreaAdapterInitializer : IHostAdapterInitializer
    {
        public void Initialize()
        {
            new BancreaSettingManager().Initialize();
        }

    }
}
