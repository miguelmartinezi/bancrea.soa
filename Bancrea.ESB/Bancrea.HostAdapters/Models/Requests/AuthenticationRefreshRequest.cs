﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bancrea.HostAdapters.Infraestructure;
using Bancrea.ESB.Core.Domain.ESB.Entities;

namespace Bancrea.HostAdapters.Models.Requests
{
    public class AuthenticationRefreshRequest : BancreaRequest
    {
        public override string END_POINT { get { return ""; } }
        public override Method HTTP_METHOD { get { return RestSharp.Method.POST; } }

        public override bool IsAuthRequest { get { return true; } }

        public string Token { get; set; }
        public string GrantType { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }


        public override BancreaParameterBag ParameterBag
        {
            get
            {
                return new BancreaParameterBag(
                    new BancreaParameter("refresh_token", Token, BancreaParameterType.RequestBody),
                    new BancreaParameter("grant_type", GrantType, BancreaParameterType.RequestBody),
                    new BancreaParameter("client_id", ClientId, BancreaParameterType.RequestBody),
                    new BancreaParameter("client_secret", ClientSecret, BancreaParameterType.RequestBody)
                    );
            }
        }

        public override dynamic ParseResponse(dynamic response)
        {
            AuthResponse authResponse = new AuthResponse();

            authResponse.Token = response.access_token;
            authResponse.TokenType = response.token_type;
            authResponse.ExpiresIn = response.expires_in;
            authResponse.RefreshToken = response.refresh_token;
            authResponse.ClientId = response.client_id;
            authResponse.UserName = response.userName;
            authResponse.Issued = response[".issued"];
            authResponse.Expires = response[".expires"];

            return authResponse;
        }
    }
}
