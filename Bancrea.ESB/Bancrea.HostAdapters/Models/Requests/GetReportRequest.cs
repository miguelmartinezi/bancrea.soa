﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.HostAdapters.Infraestructure;
using Bancrea.HostAdapters.Infraestructure.Exceptions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Models.Requests
{
    public class GetReportRequest : BancreaRequest
    {
        public override string END_POINT { get { return "/report"; } }
        public override Method HTTP_METHOD { get { return RestSharp.Method.GET; } }

        public string CompanyId { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime FinishedDate { get; set; }

        public override BancreaParameterBag ParameterBag
        {
            get
            {
                return new BancreaParameterBag(
                    new BancreaParameter("company", CompanyId, BancreaParameterType.QueryString),
                    new BancreaParameter("startedDate", StartedDate, BancreaParameterType.QueryString),
                    new BancreaParameter("finishedDate", FinishedDate, BancreaParameterType.QueryString)
                    );
            }
        }

        public override dynamic ParseResponse(dynamic response)
        {
            List<Report> reports = new List<Report>();

            try
            {
                foreach (dynamic reportResponse in response.ReportList)
                {
                    Report report = new Report();

                    report.CodRet = reportResponse.CodRet;
                    report.NomEmpresa = reportResponse.NomEmpresa;
                    report.FechaHoy = reportResponse.FechaHoy;
                    report.Sucursal = reportResponse.Sucursal;
                    report.NomSucursal = reportResponse.NomSucursal;
                    report.Plaza = reportResponse.Plaza;
                    report.NomPlaza = reportResponse.NomPlaza;
                    report.Cuenta = reportResponse.Cuenta;
                    report.Secuencia = reportResponse.Secuencia;
                    report.NumCliente = reportResponse.NumCliente;
                    report.NomCliente = reportResponse.NomCliente;
                    report.FechaPerPag = reportResponse.FechaPerPag;
                    report.InteresPerPag = reportResponse.InteresPerPag;
                    report.CtaCheques = reportResponse.CtaCheques;
                    report.FechaAlta = reportResponse.FechaAlta;
                    report.FechaVenc = reportResponse.FechaVenc;
                    report.Tasa = reportResponse.Tasa;
                    report.Plazo = reportResponse.Plazo;
                    report.Capital = reportResponse.Capital;
                    report.Interes = reportResponse.Interes;
                    report.Isr = reportResponse.Isr;
                    report.Moneda = reportResponse.Moneda;
                    report.PlazoPerPag = reportResponse.PlazoPerPag;
                    report.NomSucursalInv = reportResponse.NomSucursalInv;
                    report.TipoMoneda = reportResponse.TipoMoneda;
                    report.NomProducto = reportResponse.NomProducto;
                    report.Producto = reportResponse.Producto;
                    report.GpoProducto = reportResponse.GpoProducto;
                    report.ProductoSubPro = reportResponse.ProductoSubPro;
                    report.FolioCampana = reportResponse.FolioCampana;
                    report.PeriodoCampana = reportResponse.PeriodoCampana;


                    reports.Add(report);

                }
            }
            catch (Exception e)
            {
                throw new ParseException("Parse Error on " + END_POINT, e);
            }

            return reports;
        }

    }
}
