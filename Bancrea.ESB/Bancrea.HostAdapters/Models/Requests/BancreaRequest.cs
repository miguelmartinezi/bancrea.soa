﻿using Bancrea.HostAdapters.Infraestructure;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Models.Requests
{
    public abstract class BancreaRequest
    {
        public abstract string END_POINT { get; }
        public abstract Method HTTP_METHOD { get; }
        public abstract BancreaParameterBag ParameterBag { get; }

        public virtual bool IsAuthRequest { get; }

        public abstract dynamic ParseResponse(dynamic response);

        public IRestRequest RestRequest
        {
            get
            {
                return RestRequestBuilder.BuildRequest(this);
            }
        }
    }
}
