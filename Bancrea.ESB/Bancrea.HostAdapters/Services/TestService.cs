﻿using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Domain.Global.Test.Model.Entities;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Services
{
    public class TestService : ITestService
    {
        public string Name { get; set; }
        public List<Person> MyPersonList { get; set; }

        public TestService(TestArguments args)
        {
            Name = args.Name;
        }
        public List<Person> GetPersons()
        {
            MyPersonList = new List<Person>();

            MyPersonList.Add(new Person
            {
                Id = 1,
                Name = "Javier",
                LastName = "Sifuentes"
            });

            MyPersonList.Add(new Person
            {
                Id = 2,
                Name = "Jose",
                LastName = "Hutchinson"
            });

            MyPersonList.Add(new Person
            {
                Id = 3,
                Name = "Alex",
                LastName = "Nava"
            });


            MyPersonList.Add(new Person
            {
                Id = 4,
                Name = "Miguel",
                LastName = "Martinez"
            });

            return MyPersonList;
        }

        public Person Error()
        {
            var i = 0;
            var div = 20 / i;

            return new Person();
        }
    }
}
