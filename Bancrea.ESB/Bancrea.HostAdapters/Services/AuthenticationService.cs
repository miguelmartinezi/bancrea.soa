﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Infrastructure;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.HostAdapters.Infraestructure;
using Bancrea.HostAdapters.Models.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private ISessionContext _session;
        private readonly RequestDispatcher _requestDispatcher;

        public AuthenticationService()
        {
            _requestDispatcher = new RequestDispatcher(true);

        }
        public AuthenticationService(ISessionContext session)
        {
            _session = session;
            _requestDispatcher = new RequestDispatcher(true);
        }

        public AuthResponse Authenticate(string userId, string password, string clientId, string clientSecret)
        {
            AuthenticationRequest request = new AuthenticationRequest()
            {
                UserId = userId,
                Password = password,
                GrantType = NancyItemsKeys.ApiGrantType,
                ClientId = clientId,
                ClientSecret = clientSecret
            };

            return _requestDispatcher.Execute(request);
        }

        public AuthResponse AuthenticateRefresh(string token, string clientId, string clientSecret)
        {
            AuthenticationRefreshRequest request = new AuthenticationRefreshRequest()
            {
                Token = token,
                ClientId = clientId,
                GrantType = NancyItemsKeys.ApiGrantTypeRefresh,
                ClientSecret = clientSecret
            };

            return _requestDispatcher.Execute(request);
        }

    }
}
