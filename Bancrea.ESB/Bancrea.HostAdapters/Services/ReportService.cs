﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Domain.Global.Test.Model.Entities;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.HostAdapters.Infraestructure;
using Bancrea.HostAdapters.Models.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.HostAdapters.Services
{
    public class ReportService : IReportService
    {
        private readonly string _token;
        private readonly RequestDispatcher _requestDispatcher;
        private RestRequestBuilder RestRequestBuilder = new RestRequestBuilder();

        public ReportService(ReportArguments args)
        {
            _token = args.Token;
            _requestDispatcher = new RequestDispatcher(_token);

        }

        public List<Report> GetReport(GetReportParams  paramBag)
        {
            GetReportRequest request = new GetReportRequest()
            {
                CompanyId = paramBag.CompanyId,
                StartedDate = paramBag.StartedDate,
                FinishedDate = paramBag.FinishedDate
            };


            return _requestDispatcher.Execute(request);
        }
    }
}
