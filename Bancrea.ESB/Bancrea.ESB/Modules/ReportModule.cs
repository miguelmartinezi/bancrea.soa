﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.ESB.Core.Domain.Global.Model;
using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Infrastructure.Exceptions;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.ESB.Infrastructure.Nancy;
using Nancy;
using Nancy.ModelBinding;
using StructureMap;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bancrea.ESB.Modules
{
    public class ReportModule : NancyModule
    {
        public ReportModule(IContainer container) : base("/report")
        {

            Post["/investment"] = _ =>
            {              
                var lookup = this.Bind<LookupObject<List<Report>, GetReportParams>>();
                lookup.Response = GetService(container).GetReport(lookup.ParameterList);                
                return Response.AsJson(lookup);
            };
        }


        private IReportService GetService(IContainer container)
        {
            var token = Context == null ? string.Empty : Context.ApiHeader().Token;
            var args = new ExplicitArguments();
            args.SetArg("args", new ReportArguments { Token = token });

            try
            {
                return container.GetInstance<IReportService>(args);
            }
            catch (StructureMapException e)
            {
                // ServiceException can be thrown from constructors in our own code when StructureMap instantiates
                // a class.
                if (e.InnerException is ServiceException)
                {
                    throw e.InnerException;
                }

                throw e;
            }
        }

    }
}