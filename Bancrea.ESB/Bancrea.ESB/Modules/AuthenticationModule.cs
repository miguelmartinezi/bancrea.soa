﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.ESB.Core.Domain.Global.Model;
using Bancrea.ESB.Core.Domain.Global.Services;
using Nancy;
using Nancy.ModelBinding;

namespace Bancrea.ESB.Modules
{
    public class AuthenticationModule : NancyModule
    {
        public AuthenticationModule(IAuthenticationService service)
            : base("/authenticate")
        {
            Post["/"] = _ =>
            {
                var credentials = this.Bind<Credentials>();
                var authResponse = service.Authenticate(credentials.UserId, credentials.Password, credentials.ClientId, credentials.ClientSecret);

                return authResponse == null ? HttpStatusCode.Unauthorized : Response.AsJson(authResponse);
            };

            Post["/refreshtoken"] = _ =>
            {
                var authRefresh = this.Bind<AuthRefresh>();
                var authResponse = service.AuthenticateRefresh(authRefresh.RefreshToken, authRefresh.ClientId, authRefresh.ClientSecret);

                return authResponse == null ? HttpStatusCode.Unauthorized : Response.AsJson(authResponse);
            };

        }


    }
}