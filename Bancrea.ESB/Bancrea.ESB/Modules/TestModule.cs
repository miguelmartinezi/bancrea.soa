﻿using Bancrea.ESB.Core.Domain.Global.Model;
using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Domain.Global.Test.Model.Entities;
using Bancrea.ESB.Core.Infrastructure.Exceptions;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.ESB.Infrastructure.Nancy;
using Nancy;
using Nancy.ModelBinding;
using StructureMap;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bancrea.ESB.Modules
{
    public class TestModule : NancyModule
    {
        public TestModule(IContainer container) : base("/test")
        {

            Get["/persons"] = _ =>
            {              
                var lookup = this.Bind<LookupObject<List<Person>, dynamic>>();
                lookup.Response = GetService(container).GetPersons();
                
                return Response.AsJson(lookup);
            };

            Get["/error"] = _ =>
            {
                var lookup = new LookupObject<Person, dynamic>();
                lookup.Response = GetService(container).Error();
                return Response.AsJson(lookup);
            };

        }


        private ITestService GetService(IContainer container)
        {
            var args = new ExplicitArguments();
            args.SetArg("args", new TestArguments { Name = "MyName"});

            try
            {
                return container.GetInstance<ITestService>(args);
            }
            catch (StructureMapException e)
            {
                // ServiceException can be thrown from constructors in our own code when StructureMap instantiates
                // a class.
                if (e.InnerException is ServiceException)
                {
                    throw e.InnerException;
                }

                throw e;
            }
        }

    }
}