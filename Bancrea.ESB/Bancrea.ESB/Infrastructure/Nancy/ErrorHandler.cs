﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ErrorHandling;
using Nancy.Extensions;
using Newtonsoft.Json;

namespace Bancrea.ESB.Infrastructure.Nancy
{
    public class ErrorHandler : IStatusCodeHandler
    {
        private readonly HttpStatusCode[] _handledStatusCodes;
        

        public ErrorHandler()
        {
            // list all http responses that should be handled
            _handledStatusCodes = new[]
                {
                    HttpStatusCode.NotFound,
                    HttpStatusCode.InternalServerError
                };
        }
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return _handledStatusCodes.Any(code => code == statusCode);
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            var error = new HttpError { HttpStatusCode = statusCode };


            //TODO manage the all diferent HttpStatusCode at future
            switch (statusCode)
            {
                case HttpStatusCode.NotFound:
                    error.Message = context.Request.Path + "not found"; //Add costant or resx
                    break;
                case HttpStatusCode.InternalServerError:
                    error.Status = "Unhandled API Exception";
                    error.Message = context.GetExceptionDetails();
                    break;
                case HttpStatusCode.Continue:
                    break;
                case HttpStatusCode.SwitchingProtocols:
                    break;
                case HttpStatusCode.Processing:
                    break;
                case HttpStatusCode.Checkpoint:
                    break;
                case HttpStatusCode.OK:
                    break;
                case HttpStatusCode.Created:
                    break;
                case HttpStatusCode.Accepted:
                    break;
                case HttpStatusCode.NonAuthoritativeInformation:
                    break;
                case HttpStatusCode.NoContent:
                    break;
                case HttpStatusCode.ResetContent:
                    break;
                case HttpStatusCode.PartialContent:
                    break;
                case HttpStatusCode.MultipleStatus:
                    break;
                case HttpStatusCode.IMUsed:
                    break;
                case HttpStatusCode.MultipleChoices:
                    break;
                case HttpStatusCode.MovedPermanently:
                    break;
                case HttpStatusCode.Found:
                    break;
                case HttpStatusCode.SeeOther:
                    break;
                case HttpStatusCode.NotModified:
                    break;
                case HttpStatusCode.UseProxy:
                    break;
                case HttpStatusCode.SwitchProxy:
                    break;
                case HttpStatusCode.TemporaryRedirect:
                    break;
                case HttpStatusCode.ResumeIncomplete:
                    break;
                case HttpStatusCode.BadRequest:
                    break;
                case HttpStatusCode.Unauthorized:
                    break;
                case HttpStatusCode.PaymentRequired:
                    break;
                case HttpStatusCode.Forbidden:
                    break;
                case HttpStatusCode.MethodNotAllowed:
                    break;
                case HttpStatusCode.NotAcceptable:
                    break;
                case HttpStatusCode.ProxyAuthenticationRequired:
                    break;
                case HttpStatusCode.RequestTimeout:
                    break;
                case HttpStatusCode.Conflict:
                    break;
                case HttpStatusCode.Gone:
                    break;
                case HttpStatusCode.LengthRequired:
                    break;
                case HttpStatusCode.PreconditionFailed:
                    break;
                case HttpStatusCode.RequestEntityTooLarge:
                    break;
                case HttpStatusCode.RequestUriTooLong:
                    break;
                case HttpStatusCode.UnsupportedMediaType:
                    break;
                case HttpStatusCode.RequestedRangeNotSatisfiable:
                    break;
                case HttpStatusCode.ExpectationFailed:
                    break;
                case HttpStatusCode.ImATeapot:
                    break;
                case HttpStatusCode.EnhanceYourCalm:
                    break;
                case HttpStatusCode.UnprocessableEntity:
                    break;
                case HttpStatusCode.Locked:
                    break;
                case HttpStatusCode.FailedDependency:
                    break;
                case HttpStatusCode.UnorderedCollection:
                    break;
                case HttpStatusCode.UpgradeRequired:
                    break;
                case HttpStatusCode.TooManyRequests:
                    break;
                case HttpStatusCode.NoResponse:
                    break;
                case HttpStatusCode.RetryWith:
                    break;
                case HttpStatusCode.BlockedByWindowsParentalControls:
                    break;
                case HttpStatusCode.ClientClosedRequest:
                    break;
                case HttpStatusCode.NotImplemented:
                    break;
                case HttpStatusCode.BadGateway:
                    break;
                case HttpStatusCode.ServiceUnavailable:
                    break;
                case HttpStatusCode.GatewayTimeout:
                    break;
                case HttpStatusCode.HttpVersionNotSupported:
                    break;
                case HttpStatusCode.VariantAlsoNegotiates:
                    break;
                case HttpStatusCode.InsufficientStorage:
                    break;
                case HttpStatusCode.BandwidthLimitExceeded:
                    break;
                case HttpStatusCode.NotExtended:
                    break;
                default:
                    break;
            }

            context.Response = JsonConvert.SerializeObject(error);

            //explicity set the content type here as the bootstrap set is ignored for errores
            //DO NOT alter the response afther this line or the content type will be get overriden.
            context.Response.ContentType = "application/json";

            context.Response.StatusCode = statusCode;
        }
    }
}