﻿using System;
using System.Collections.Generic;
using System.Web;
using StructureMap;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.StructureMap;
using Nancy.Serialization.JsonNet;
using Nancy.Session;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using Bancrea.ESB.Core.Infrastructure.Domain.ESB;
using System.Diagnostics;
using Bancrea.ESB.Core.Infrastructure;
using System.IO;
using System.Threading;
using System.Globalization;
using Bancrea.ESB.Core.Infrastructure.Exceptions;
using System.Data.Entity;
using Bancrea.ESB.Core.Infrastructure.Data;
using Bancrea.ESB.Core.Migrations;

namespace Bancrea.ESB.Infrastructure.Nancy
{
    public class Bootstraper : StructureMapNancyBootstrapper
    {
        private NancyInternalConfiguration internalConfiguration;


        /// <summary>
        /// Configure application container
        /// </summary>
        /// <param name="existingContainer">IContainer</param>
        protected override void ConfigureApplicationContainer(IContainer existingContainer)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BancreaESBDataContext, Configuration>());
            base.ConfigureApplicationContainer(existingContainer);
            HttpContext.Current.Application.Add("StartupTime", DateTime.Now.ToString());
            existingContainer.Configure(registry =>
            {
                registry.AddRegistry(new ApplicationDependencyRegistry());
            });

            InitializeHostAdapters(existingContainer);
            AdapterFactory.LoadAdapters(existingContainer);
        }

        /// <summary>
        /// Configure request container
        /// </summary>
        /// <param name="container">IContainer</param>
        /// <param name="context">NancyContext</param>
        protected override void ConfigureRequestContainer(IContainer container, NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);
            container.Configure(registry =>
            {
                registry.AddRegistry(new SessionDependencyRegistry());
            });
        }

        /// <summary>
        /// Request Startup
        /// </summary>
        /// <param name="container">IContainer</param>
        /// <param name="pipelines">IPipelines</param>
        /// <param name="context">NancyContext</param>
        protected override void RequestStartup(IContainer container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            //Here Configure any other Startup required at Pipelines
            ConfigureSession(container, pipelines);
            
        }

        /// <summary>
        /// Nancy Internal Configuration
        /// </summary>
        protected override NancyInternalConfiguration InternalConfiguration
        {
            get
            {
                if (internalConfiguration == null)
                {
                    internalConfiguration = base.InternalConfiguration;
                    internalConfiguration.StatusCodeHandlers.Clear();
                    internalConfiguration.StatusCodeHandlers.Add(typeof(ErrorHandler));

                    internalConfiguration.Serializers.Insert(0, typeof(JsonNetSerializer));
                }
                return internalConfiguration;
            }
        }

        /// <summary>
        /// ConfigureSession
        /// </summary>
        /// <param name="container">IContainer</param>
        /// <param name="pipelines">IPipelines</param>
        private void ConfigureSession(IContainer container, IPipelines pipelines)
        {
            pipelines.BeforeRequest += StartStopWatch;
            pipelines.BeforeRequest += context => OpenSession(container);
            pipelines.BeforeRequest += ReadRequestHeader;
            pipelines.BeforeRequest += context => ReadRequestBody(context, container);
            //pipelines.BeforeRequest += context => SetLocale(context);

            pipelines.AfterRequest += context => CloseSession(container);
            //Add here ResponseLogging in case be implemented in the future.

            pipelines.OnError += (context, ex) => RejectSession(context, ex, container);
        }

        /// <summary>
        /// Set Locale
        /// </summary>
        /// <param name="context">NancyContext</param>
        /// <returns>Response</returns>
        private Response SetLocale(NancyContext context)
        {
            if (!string.IsNullOrEmpty(context.ApiHeader().Locale))
            {
                SetLocale(context.ApiHeader().Locale, context);
            }

            return null;
        }

        /// <summary>
        /// Set the culture for this http session
        /// 
        /// Locale-sensitive content will be localized based on the locale specified here.
        /// </summary>
        /// <param name="locale">string</param>
        /// <param name="context">NancyContext</param>
        private void SetLocale(string locale, NancyContext context)
        {
            try
            {
                //Create two threads here, you can use UICulture for user interface culture and Current culture to define
                // user locale of the system.
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(locale);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(locale);
            }
            catch (CultureNotFoundException ex)
            {
                throw new ServiceException("Invalid locale" + locale, ex);
            }
        }

        /// <summary>
        /// An anchor point to start the performance measurement of the API service call.
        /// </summary>
        /// <param name="context">NancyContext</param>
        /// <returns>Response</returns>
        private Response StartStopWatch(NancyContext context)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            context.Items.Add(NancyItemsKeys.ApiStopwatch, stopWatch);
            return null;
        }

        private Response ReadRequestHeader(NancyContext context)
        {
            var header = context.Request.Headers[Keys.ApiHeader] as string[];
            if (header != null && header.Length == 0)
            {
                return new Response().AsHttpError(HttpStatusCode.BadRequest, string.Empty, "Request header missing.");
            }
            return null;
        }

        /// <summary>
        /// Extrar the request body and store it
        /// </summary>
        /// <param name="context">NancyContext</param>
        /// <param name="container">IContainer</param>
        /// <returns>Response</returns>
        private Response ReadRequestBody(NancyContext context, IContainer container)
        {
            string requestBody = string.Empty;

            try
            {
                using (StreamReader streamReader = new StreamReader(context.Request.Body))
                {
                    requestBody = streamReader.ReadToEnd();
                }
                context.Items.Add(NancyItemsKeys.ApiRequestBody, requestBody);
            }
            catch (Exception ex)
            {
                //Use here WebApiLogUtility????
                Console.WriteLine(ex);
            }
            context.Request.Body.Position = 0;

            return null;
        }

        /// <summary>
        /// Open Session
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        private Response OpenSession(IContainer container)
        {
            return null;
        }

        /// <summary>
        /// After Pipeline
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        private AfterPipeline CloseSession(IContainer container)
        {
            var session = container.GetInstance<ISessionContext>();
            session.Dispose();
            return null;
        }

        /// <summary>
        /// RejectSession
        /// </summary>
        /// <param name="context">NancyContext</param>
        /// <param name="exception">Exception</param>
        /// <param name="container">IContainer</param>
        /// <returns>Response</returns>
        private Response RejectSession(NancyContext context, Exception exception, IContainer container)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(exception);

            var session = container.GetInstance<ISessionContext>();
            session.HasErrors = true;
            session.Dispose();

            var serviceException = exception as ServiceException;
            if (serviceException != null)
            {
                var fault = serviceException.ServiceFault;

                //TODO:Should this be a 502 Bad Gatewat error?
                Response response = new Response().AsHttpError(HttpStatusCode.BadGateway, fault.ErrorId, fault.Status, fault.Message,
                    fault.Details);

                //Log the error here in case you implement other log.

                return response;
            }

            return null;
        }


        /// <summary>
        /// Initialize Host Adapter
        /// </summary>
        /// <param name="container">IContainer</param>
        private void InitializeHostAdapters(IContainer container)
        {
            var hostAdaptersInitializers = container.GetAllInstances<IHostAdapterInitializer>();

            foreach (var initializer in hostAdaptersInitializers)
            {
                initializer.Initialize();
            }
        }

    }
}