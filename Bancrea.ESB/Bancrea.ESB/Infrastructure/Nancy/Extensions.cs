﻿using Bancrea.ESB.Core.Infrastructure;
using Bancrea.ESB.Shared.Api;
using Nancy;
using Nancy.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bancrea.ESB.Infrastructure.Nancy
{
    public static class Extensions
    {
        public static ISerializer jsonSerializer;

        public static Response AsHttpError(this IResponseFormatter formatter, HttpStatusCode statusCode, string statusDescription = null, 
            string message = null, object errorDetails = null)
        {
            var serializer = jsonSerializer ?? (jsonSerializer = formatter.Serializers.FirstOrDefault(s => s.CanSerialize("application/json")));

            var error = new HttpError { HttpStatusCode = statusCode, Status = statusDescription, Message = message,
                Details = errorDetails };

            var response = new JsonResponse(error, serializer);
            response.StatusCode = statusCode;

            return response;
        }

        public static Response AsHttpError(this Response response, HttpStatusCode statusCode, string errorId, string statusDescription = null, string message = null, 
            object errorDetails = null)
        {
            var error = new HttpError { HttpStatusCode = statusCode, ErrorId = errorId, Status = statusDescription, Message = message, Details = errorDetails };

            response = JsonConvert.SerializeObject(error);
            response.StatusCode = statusCode;

            return response;
        }


        public static ApiHeader ApiHeader(this NancyContext context)
        {
            ApiHeader apiHeader = new ApiHeader();
            if (!context.Items.ContainsKey(NancyItemsKeys.ApiHeader))
            {
                var header = context.Request.Headers[Keys.ApiAuthorizationHeader].FirstOrDefault();
                apiHeader.Token = header;

            }

            return apiHeader;
        }
    }
}