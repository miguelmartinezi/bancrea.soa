﻿using Nancy;


namespace Bancrea.ESB.Infrastructure.Nancy
{
    /// <summary>
    /// This class contains the error information formatted in the body of a http error.
    /// The fields here should be generic enough for any applications consuming the services on this Web API to populate it.
    /// </summary>
    public class HttpError
    {
        /// <summary>
        /// The http status code for this error.
        /// 
        /// TODO: is this field needed?
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// The status of the service.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The error message returned by the service.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Details of the error. The details here will be specific to the host application.
        /// </summary>
        public object Details { get; set; }

        /// <summary>
        /// Id that identifies the error
        /// </summary>
        public object ErrorId { get; set; }

        public HttpError() { }

        public HttpError(HttpStatusCode httpStatusCode, string status, string message, string errorId, object details = null)
        {
            HttpStatusCode = httpStatusCode;
            ErrorId = errorId;
            Status = status;
            Message = message;
            Details = details;
        }
    }
}