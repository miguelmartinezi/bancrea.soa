﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.ESB.Model.Events.Base
{
    public enum ResourceState
    {
        Setup,
        Run,
        Down,
        Idle,
        Unknow
    }
}
