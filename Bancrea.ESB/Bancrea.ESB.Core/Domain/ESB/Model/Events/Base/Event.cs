﻿using Bancrea.ESB.Core.Domain.ESB.Model.EventAdapters.Base;
using Bancrea.ESB.Core.Infrastructure.Domain.ESB;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.ESB.Model.Events.Base
{
    public class Event : IEvent
    {
        private readonly EventObservatory _tracker;

        protected bool InitialazeOnConstruction{ get; set; }
        protected IEventAdapter DefaultAdapter { get; set; }

        public string Code { get; set; }

        public string Id { get; set; }

        // State of the resource that this event can be submitted to.
        public ResourceState[] EnabledStates { get; set; }

        // If and when events support additional operations, they can be added here and fired on Event.Update
        internal EventOperation CurrentOperation { get; set; }

        internal enum EventOperation
        {
            Submit
        }

        protected Event(bool autoInitialiaze = false)
        {
            Id = GetType().Name;
            InitialazeOnConstruction = autoInitialiaze;

            _tracker = new EventObservatory();

            if (!ApplicationContext.EventAdapterCache.ContainsKey(Id))
            {
                throw new InstanceNotFoundException("There are not adapters defined for the event:" + Id);
            }

            foreach (var adapterType in ApplicationContext.EventAdapterCache[Id])
            {
                var adapter = AdapterFactory.Create(adapterType);
                _tracker.Subscribe(adapter);

            }

            if (DefaultAdapter == null)
            {
                throw new InstanceNotFoundException("There is not default adapter defined for event:" + Id);
            }

        }

        public void Initialize()
        {
            DefaultAdapter.Initialize(this);
        }


        public void Update()
        {
            _tracker.UpdateSubscribers(this);
        }

        public bool Enabled(string currState)
        {
            ResourceState state;
            if (Enum.TryParse(currState, out state) && EnabledStates != null)
            {
                return EnabledStates.Contains(state);
            }

            return false;
        }
    }
}
