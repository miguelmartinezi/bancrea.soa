﻿using Bancrea.ESB.Core.Domain.ESB.Model.Events.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.ESB.Model.EventAdapters.Base
{
    public abstract class EventAdapter : IEventAdapter, IObserver<Event>
    {
        protected Event Event { get; set; }

        public void OnNext(Event pEvent)
        {
            Event = pEvent;
            switch (pEvent.CurrentOperation)
            {
                case Event.EventOperation.Submit:
                    break;
                default:
                    break;
            }
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public abstract void Initialize(Event pEvent);
        public abstract void Submit(Event pEvent);
    }
}
