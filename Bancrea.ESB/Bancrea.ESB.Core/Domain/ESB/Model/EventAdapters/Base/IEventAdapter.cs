﻿using Bancrea.ESB.Core.Domain.ESB.Model.Events.Base;


namespace Bancrea.ESB.Core.Domain.ESB.Model.EventAdapters.Base
{
    public interface IEventAdapter
    {
        void Initialize(Event pEvent);
        void Submit(Event pEvent);
    }
}
