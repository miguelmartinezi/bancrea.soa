﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Model
{
    public class LookupObject<T, P>
        where T : new()
        where P : new()
    {
        // Should these be here?
        //public string Command { get; set; }
        //public bool PreProcess { get; set; }
        //public bool PostProcess { get; set; }
        public T Response { get; set; }
        public P ParameterList { get; set; }

        public LookupObject()
        {
            Response = new T();
            ParameterList = new P();
        }
    }
}
