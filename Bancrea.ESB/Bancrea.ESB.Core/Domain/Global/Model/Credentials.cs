﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Model
{
    public class Credentials
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        public string Password { get; set; }
        [Key, Column(Order = 1)]
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
