﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Model
{
    public class HostSetting
    {
        [Key]
        public string Name { get; set; }

        [NotMapped]
        public string Description { get; set; }

        public string DescriptionKey { get; set; }

        public string CurrentValue { get; set; }

        public string DefaultValue { get; set; }

        public string Type { get; set; }

        public bool IsNew { get; set; }
    }

    public struct HostSettingType
    {
        public const string Basic = "Basic";
        public const string Advanced = "Advanced";
    }
}
