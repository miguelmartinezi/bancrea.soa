﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using Bancrea.ESB.Core.Domain.Global.Test.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Services
{
    public interface IReportService
    {
        List<Report> GetReport(GetReportParams paramBag);
    }

    public class GetReportParams
    {
        public string CompanyId { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime FinishedDate { get; set; }
    }

}
