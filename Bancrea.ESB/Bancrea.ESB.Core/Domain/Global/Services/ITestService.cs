﻿using Bancrea.ESB.Core.Domain.Global.Test.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Services
{
    public interface ITestService
    {
        List<Person> GetPersons();
        Person Error();
    }
}
