﻿using Bancrea.ESB.Core.Domain.ESB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Domain.Global.Services
{
    public interface IAuthenticationService
    {
        AuthResponse Authenticate(string userId, string password, string clientId, string clientSecret);
        AuthResponse AuthenticateRefresh(string token, string clientId, string clientSecret);
    }
}
