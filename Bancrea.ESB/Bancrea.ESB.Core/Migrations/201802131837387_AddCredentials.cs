namespace Bancrea.ESB.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCredentials : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Credentials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Credentials");
        }
    }
}
