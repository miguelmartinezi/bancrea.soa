namespace Bancrea.ESB.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HostSettings",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        DescriptionKey = c.String(),
                        CurrentValue = c.String(),
                        DefaultValue = c.String(),
                        Type = c.String(),
                        IsNew = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Name);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HostSettings");
        }
    }
}
