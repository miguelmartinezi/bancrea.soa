namespace Bancrea.ESB.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHostSetting : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Credentials");
            AddColumn("dbo.Credentials", "ClientId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Credentials", "ClientSecret", c => c.String());
            AlterColumn("dbo.Credentials", "UserId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Credentials", new[] { "UserId", "ClientId" });
            DropColumn("dbo.Credentials", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Credentials", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Credentials");
            AlterColumn("dbo.Credentials", "UserId", c => c.String());
            DropColumn("dbo.Credentials", "ClientSecret");
            DropColumn("dbo.Credentials", "ClientId");
            AddPrimaryKey("dbo.Credentials", "Id");
        }
    }
}
