﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure
{

    public struct Keys
    {
        public const string ApiHeader = "X-EsbApi";
        public const string ApiAuthorizationHeader = "Authorization";
    }

    public struct NancyItemsKeys
    {
        public const string ApiHeader = "ApiHeader";
        public const string ApiRequestBody = "WebApiRequestBody";
        public const string ApiStopwatch = "WebApiStopwatch";
        public const string ApiGrantType = "password";
        public const string ApiGrantTypeRefresh = "refresh_token";

    }

}
