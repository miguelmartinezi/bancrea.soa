﻿using Bancrea.ESB.Core.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceFault ServiceFault { get; private set; }

        public ServiceException(ServiceFault pFault, Exception innerException)
            : base(pFault.Message, innerException)
        {
            ServiceFault = pFault;
        }

        public ServiceException(string msg)
             : this(new ServiceFault
             {
                 Message = msg
             })
        {
            // Empty constructor.
        }

        public ServiceException(ServiceFault pFault)
            : base(pFault.Message)
        {
            ServiceFault = pFault;
        }

        public ServiceException(Exception innerException)
            : this(new ServiceFault
            {
                Message = innerException.Message,
                Details = innerException.StackTrace
            }, innerException)
        {
            // Left empty on purpose.
        }

        public ServiceException(string msg, Exception innerException)
            : this(new ServiceFault
            {
                Message = msg,
                Details = innerException.Message + " " + innerException.StackTrace
            }, innerException)
        {
            // Left empty on purpose.
        }


    }
}
