﻿using Bancrea.ESB.Core.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.Exceptions
{
    /// <summary>
    /// This exception class represents a legitimate service error, not an unanticipated system issue in the Web API.
    /// </summary>
    public class ServiceErrorException : ServiceException
    {
        public ServiceErrorException(ServiceFault fault)
            : base(fault)
        {
        }
    }
}
