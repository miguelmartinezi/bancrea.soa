﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bancrea.ESB.Core.Infrastructure.Exceptions
{
    public class ServiceFault
    {
        // Make this enum? 
        public const string ErrorStatus = "Error";

        public string Status { get; set; }
        public string RequestId { get; set; }
        public virtual string ErrorId { get; set; }
        public virtual string Message { get; set; }
        public virtual object Details { get; set; }

        public ServiceFault()
        {
            Status = ErrorStatus;
        }
    }
}