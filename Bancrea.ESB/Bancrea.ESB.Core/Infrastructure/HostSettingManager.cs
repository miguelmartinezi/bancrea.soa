﻿using Bancrea.ESB.Core.Domain.Global.Model;
using Bancrea.ESB.Core.Infrastructure.Data;
using Bancrea.ESB.Shared.Models;
using Bancrea.ESB.Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure
{
    /// <summary>
    /// HostSettingManager class manages the settings in the Web API. It is responsible for seeding, updating and deleting the settings.
    /// 
    /// TODO: 
    /// Pull out common logic from AppSettingManager and create a base/composite class in the shared project.
    /// </summary>
    public abstract class HostSettingManager
    {
        /// <summary>
        /// A list of setting names.
        /// </summary>
        protected IList<string> SettingNames;

        /// <summary>
        /// The host settings.
        /// </summary>
        protected abstract IList<HostSetting> HostSettings { get; }


        public HostSettingManager()
        {
            SettingNames = HostSettings.Select(s => s.Name).ToList();
        }

        /// <summary>
        /// Initialize the settings in the database. 
        /// </summary>
        /// <param name="context"></param>
        public void Initialize()
        {
            Seed(new BancreaESBDataContext());
        }

        /// <summary>
        /// Return the settings based on the specified parameters.
        /// </summary>
        /// <param name="parameters">An implementation of IQueryParameters which provides the information of pagination and filtering.</param>
        /// <returns>The paginated data and the pagination information.</returns>
        public PagedData<HostSetting> GetSettingList(IQueryParameters parameters)
        {
            BancreaESBDataContext dataContext = new BancreaESBDataContext();
            PagedData<HostSetting> settings = DbUtility.GetEntities<HostSetting>(dataContext.HostSettings, parameters);
            PopulateDescriptions(settings.Data);
            return settings;
        }

        /// <summary>
        /// Update the current value of the setting.
        /// </summary>
        /// <param name="key">The setting name.</param>
        /// <param name="value">The new current value.</param>
        public void UpdateSettingValue(string key, string value)
        {
            BancreaESBDataContext dataContext = new BancreaESBDataContext();
            // TODO: Candidate to be cached in the memory to avoid excessive database access.
            var setting = dataContext.HostSettings.Where(a => a.Name == key).Single();
            setting.CurrentValue = value;
            dataContext.SaveChanges();
        }

        /// <summary>
        /// Return the specified setting.
        /// </summary>
        /// <param name="setting">The setting name.</param>
        /// <returns>The requested setting.</returns>
        public HostSetting GetSetting(HostSetting setting)
        {
            BancreaESBDataContext DataContext = new BancreaESBDataContext();
            // TODO: Candidate to be cached in the memory to avoid excessive database access.
            HostSetting result = DataContext.HostSettings.Where(a => a.Name == setting.Name).Single();
            PopulateDescription(result);
            return result;
        }

        /// <summary>
        /// Return the current value of a specified setting.
        /// </summary>
        /// <param name="key">The name of the setting.</param>
        /// <returns>The current value of the setting.</returns>
        public static string GetSetting(string key)
        {
            BancreaESBDataContext DataContext = new BancreaESBDataContext();
            // TODO: Candidate to be cached in the memory to avoid excessive database access.
            var setting = DataContext.HostSettings.Where(a => a.Name == key).Single();
            return setting.CurrentValue;
        }

        /// <summary>
        /// Seed the database.
        /// </summary>
        /// <param name="context"></param>
        private void Seed(BancreaESBDataContext context)
        {
            AddOrUpdate(context.HostSettings);
            Clean(context.HostSettings);
            context.SaveChanges();
        }

        /// <summary>
        /// Helper to add or update the existing settings in the table.
        /// </summary>
        /// <param name="existingSettings"></param>
        private void AddOrUpdate(DbSet<HostSetting> existingSettings)
        {
            bool isNewDatabase = existingSettings.Count<HostSetting>() == 0;

            foreach (HostSetting newSetting in HostSettings)
            {
                try
                {
                    HostSetting existingSetting = existingSettings.Single(a => a.Name == newSetting.Name);
                    // Setting exists, update the properties. 
                    existingSetting.DescriptionKey = newSetting.DescriptionKey;
                    existingSetting.Type = newSetting.Type;

                    // If the current value is a custom value, do not do the update on the current value field.                    
                    if (existingSetting.CurrentValue == existingSetting.DefaultValue &&
                        existingSetting.CurrentValue != newSetting.DefaultValue)
                    {
                        existingSetting.CurrentValue = newSetting.DefaultValue;
                    }

                    existingSetting.DefaultValue = newSetting.DefaultValue;
                }
                catch (InvalidOperationException e)
                {
                    // Setting doesn't exist, add it.
                    // Mark the new setting as new if this is not a new database.
                    newSetting.IsNew = !isNewDatabase;
                    existingSettings.Add(newSetting);
                }
            }
        }

        /// <summary>
        /// Clean the old settings that should no longer be in the database.
        /// </summary>
        /// <param name="settings"></param>
        private void Clean(DbSet<HostSetting> settings)
        {
            var oldSettings = settings.Where(a => !SettingNames.Contains(a.Name));

            foreach (HostSetting oldSetting in oldSettings)
            {
                settings.Remove(oldSetting);
            }
        }

        private void PopulateDescriptions(IList<HostSetting> settings)
        {
            foreach (HostSetting setting in settings)
            {
                PopulateDescription(setting);
            }
        }

        private void PopulateDescription(HostSetting setting)
        {
            setting.Description = GetMessage(setting.DescriptionKey);
        }


        /// <summary>
        /// Call this method to indicate all new settings have been acknowledged.
        /// 
        /// This method will clear out the new flags in the settings.
        /// </summary>
        public void ClearNewFlags()
        {
            BancreaESBDataContext dataContext = new BancreaESBDataContext();
            var settings = dataContext.HostSettings.Where(s => s.IsNew);
            foreach (HostSetting setting in settings.ToList())
            {
                setting.IsNew = false;
            }
            dataContext.SaveChanges();
        }

        /// <summary>
        /// Get the settings for the initialization pages.
        /// </summary>
        /// <param name="parameters">An implementation of IPagedParameters whcih provides the information of pagination.</param>
        /// <returns>It returns the basic settings and also the new advanced settings.</returns>
        public PagedData<HostSetting> GetInitSettingList(IPageParameters parameters)
        {
            BancreaESBDataContext dataContext = new BancreaESBDataContext();

            //
            // TODO: Update the DbUtility.GetEntities() to support complex filters and multiple sort columns
            // For now, we just do it manually.
            //
            var query = from x in dataContext.HostSettings
                        select x;
            query = query.Where(x => x.Type == "Basic" || (x.IsNew == true));
            query = DbUtility.SortQuery<HostSetting>(query, "Name", Bancrea.ESB.Shared.Utilities.DbUtility.SortOrder.ASC);
            PagedData<HostSetting> settings = DbUtility.Paginate<HostSetting>(query, parameters.CurrentPage, parameters.PageSize);
            PopulateDescriptions(settings.Data);
            return settings;
        }

        /// <summary>
        /// Returns the locale-sensitive labels/messages.
        /// </summary>
        /// <param name="key">The message key</param>
        /// <returns>Localized messages</returns>
        protected abstract string GetMessage(string key);
    }
}
