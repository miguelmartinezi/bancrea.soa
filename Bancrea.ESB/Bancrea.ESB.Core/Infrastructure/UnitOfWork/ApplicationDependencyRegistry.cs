﻿using System;
using StructureMap.Configuration.DSL;
using Bancrea.ESB.Core.Domain.Global.Services;
using Bancrea.ESB.Core.Domain.ESB.Model.Events.Base;

namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{

    public class ApplicationDependencyRegistry : Registry
    {
        public ApplicationDependencyRegistry()
        {
            Scan(_ =>
            {
                _.TheCallingAssembly();
                _.AddAllTypesOf<IEvent>().NameBy(t => t.Name.ToUpper());

                var path = AppDomain.CurrentDomain.BaseDirectory + @"bin\Adapters";
                _.AssembliesFromPath(path, assembly => assembly.GetName().Name.Contains("Adapter"));
                _.AddAllTypesOf<IHostAdapterInitializer>();
                _.AddAllTypesOf<IAuthenticationService>();
                _.AddAllTypesOf<IReportService>();
                _.AddAllTypesOf<ITestService>();
            });
        }
    }
}
