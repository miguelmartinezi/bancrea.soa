﻿using StructureMap.Configuration.DSL;


namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    public class SessionDependencyRegistry : Registry
    {
        public SessionDependencyRegistry()
        {
            For<ISessionContext>().Singleton().Use<SessionContext>();
        }
    }
}
