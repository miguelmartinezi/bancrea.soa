﻿using System;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;


namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    public class EventRegistrationConvention : IRegistrationConvention
    {
        public void Process(Type type, Registry registry)
        {
            if (type.IsAbstract || type.IsInterface || type.IsEnum)
            {
                return;
            }

            var eventInterface = type.GetInterface("IEvent");
            if (eventInterface == null)
            {
                return;
            }
        }
    }
}
