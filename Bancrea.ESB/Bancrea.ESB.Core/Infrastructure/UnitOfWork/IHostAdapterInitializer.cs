﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    /// <summary>
    /// 
    /// This interface provides the host systems a mechanism to perfrom initialization.
    /// 
    /// Host Adapter can create a concrete implementation of this interface and perform initialization here.
    /// 
    /// At application load time, the core will search for all its implementations and execute it automatically.
    /// </summary>
    public interface IHostAdapterInitializer
    {
        /// <summary>
        /// This method will be called upon application startup.
        /// </summary>
        void Initialize();
    }
}
