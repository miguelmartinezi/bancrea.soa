﻿using Bancrea.ESB.Core.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    public class SessionContext : ISessionContext
    {
        public bool HasErrors { get; set; }

        
        public BancreaESBDataContext DataStore { get; set;}


        public SessionContext()
        {
            DataStore = new BancreaESBDataContext();
        }

        public void Dispose()
        {
            if (DataStore != null)
            {
                if (!HasErrors)
                {
                    DataStore.SaveChanges();
                }

                DataStore.Dispose();
            }
        }
    }
}
