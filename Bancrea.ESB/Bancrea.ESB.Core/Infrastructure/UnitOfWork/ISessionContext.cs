﻿using Bancrea.ESB.Core.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    public interface ISessionContext : IDisposable
    {
        bool HasErrors { get; set; }
        //You may need to implement DbContext here if you want to store to DB
        BancreaESBDataContext DataStore { get; set; }
    }
}
