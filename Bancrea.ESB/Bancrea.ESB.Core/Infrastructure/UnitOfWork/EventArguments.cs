﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.UnitOfWork
{
    /// <summary>
    /// This shouldn't be required:
    /// http://stackoverflow.com/questions/13034928/error-passing-constructor-args-to-structuremap-named-instance
    /// </summary>
    public class EventArguments
    {
        public string EventId { get; set; }
        public string Description { get; set; }
        public bool InitializeOnConstruction { get; set; }

    }

    public class LookupArguments
    {
        public Guid? Token { get; set; }
        public string RoleId { get; set; }
    }

    public class ConfigArguments
    {
        //Add here event dbContext
    }

    public class EventIdParameters
    {
        public string EventCode { get; set; }

    }

    public class TestArguments
    {
        public string Name { get; set; }
    }

    public class ReportArguments
    {
        public string Token { get; set; }
    }

}
