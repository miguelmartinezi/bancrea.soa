﻿using Bancrea.ESB.Core.Domain.ESB.Model.Events.Base;
using System;
using System.Linq;

namespace Bancrea.ESB.Core.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EventAdapterAttribute : Attribute
    {
        public Type EventType { get; set; }

        public string EventId
        {
            get
            {
                return EventType.Name;
            }
        }

        public EventAdapterAttribute(Type type)
        {
            if (!type.GetInterfaces().Contains(typeof(IEvent)))
            {
                throw new ArgumentException(string.Format("Type of {0} does not appear to be a valid Event", type.Name));
            }

            EventType = type;
        }
    }
}
