﻿using Bancrea.ESB.Core.Domain.ESB.Model.EventAdapters.Base;
using Bancrea.ESB.Core.Infrastructure.Attributes;
using Bancrea.ESB.Core.Infrastructure.UnitOfWork;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Core.Infrastructure.Domain.ESB
{
    public class AdapterFactory
    {
        public static EventAdapter Create(Type adapterType)
        {
            return Activator.CreateInstance(adapterType) as EventAdapter;
        }

        public static void LoadAdapters(IContainer container) 
        {
            var adapterCache = new Dictionary<string, List<Type>>();
            var adapters = container.GetAllInstances<IEventAdapter>();

            foreach (var adapter in adapters)
            {
                var type = adapter.GetType();
                if (type.GetCustomAttributes(typeof(EventAdapterAttribute), true).Length <= 0) 
                {
                    continue;
                }

                var eventType = (type.GetCustomAttributes(typeof(EventAdapterAttribute), true).GetValue(0) as
                    EventAdapterAttribute).EventId;

                if (adapterCache.ContainsKey(eventType))
                {
                    var existingAdapterList = adapterCache[eventType];
                    existingAdapterList.Add(type);
                }
                else
                {
                    var newAdapterList = new List<Type> { type };
                    adapterCache.Add(eventType, newAdapterList);
                }
            }

            ApplicationContext.EventAdapterCache = adapterCache;
        }
    }
}
