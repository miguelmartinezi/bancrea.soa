﻿using System;
using System.Collections.Generic;
using Bancrea.ESB.Core.Domain.ESB.Model.Events.Base;

namespace Bancrea.ESB.Core.Infrastructure.Domain.ESB
{
    public class EventObservatory : IObservable<Event>
    {
        private readonly List<IObserver<Event>> _adapters;

        public EventObservatory()
        {
            _adapters = new List<IObserver<Event>>();
        }

        public IDisposable Subscribe(IObserver<Event> observer)
        {
            _adapters.Add(observer);
            return null;
        }

        public void UpdateSubscribers(Event pEvent)
        {
            foreach (var adapter in _adapters)
            {
                adapter.OnNext(pEvent);
            }
        }
    }
}
