﻿using Bancrea.ESB.Core.Domain.Global.Model;
using System.Data.Entity;

namespace Bancrea.ESB.Core.Infrastructure.Data
{
    public class BancreaESBDataContext : DbContext
    {
        public DbSet<HostSetting> HostSettings { get; set; }
        public DbSet<Credentials> Credentials { get; set; }
        public BancreaESBDataContext() : base("Bancrea.ESB")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
