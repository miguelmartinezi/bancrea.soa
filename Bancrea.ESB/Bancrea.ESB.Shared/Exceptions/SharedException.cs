﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Shared.Exceptions
{
    class WebSharedException : Exception
    {
        public WebSharedException()
        {
        }

        public WebSharedException(string message)
            : base(message)
        {
        }

        public WebSharedException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}
