﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Shared.Api
{
    public class ApiHeader : IApiHeader
    {
        public string Token { get; set; }

        public string Locale { get; set; }
    }

    public interface IApiHeader
    {
        string Token { get; set; }
    }
}
