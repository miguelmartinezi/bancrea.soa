﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Shared.Utilities.DbFilters
{
    class DbUtilityHelper
    {
        public static List<T> MakeListOfSpecificType<T>(IEnumerable<object> list)
        {
            return list.Select(x => Convert.ChangeType(x, typeof(T))).Cast<T>().ToList();
        }
    }

    public class DbFilter
    {
        public string column { get; set; }
        public FilterOp operation { get; set; }
        public List<object> values { get; set; }
    }

    public enum FilterOp
    {
        Contains,
        Equals,
        GreaterThan,
        GreaterThanEquals,
        LessThan,
        LessThanEquals
    }
}
