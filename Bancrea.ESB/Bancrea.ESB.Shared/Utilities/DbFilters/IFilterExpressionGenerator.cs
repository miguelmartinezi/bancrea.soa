﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Shared.Utilities.DbFilters
{
    interface IFilterExpressionGenerator
    {
        IEnumerable<FilterOp> OperationsApplied { get; } // determine which operation this FilterExpGenerator is applied to.
        Expression<Func<T, bool>> GenerateFilterExp<T>(DbFilter filter); // Generate the filter expression
    }
}
