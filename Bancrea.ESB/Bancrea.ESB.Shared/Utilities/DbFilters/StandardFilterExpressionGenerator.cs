﻿using Bancrea.ESB.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bancrea.ESB.Shared.Utilities.DbFilters
{
    class StandardFilterExpressionGenerator : IFilterExpressionGenerator
    {

        readonly List<FilterOp> operationsApplied = new List<FilterOp> { FilterOp.Contains, FilterOp.Equals, FilterOp.GreaterThan, FilterOp.GreaterThanEquals, FilterOp.LessThan, FilterOp.LessThanEquals };

        public IEnumerable<FilterOp> OperationsApplied { get { return operationsApplied; } }
        // This method generate an expression in the form: (r => filter.values.Any( value => compareExp( r.property, value)))
        public Expression<Func<T, bool>> GenerateFilterExp<T>(DbFilter filter)
        {
            var r = Expression.Parameter(typeof(T), "r");
            if (filter.values.Count() == 0)
            {
                return (Expression<Func<T, bool>>)Expression.Lambda(Expression.Constant(true, typeof(bool)), r);
            }

            var columnType = typeof(T).GetProperty(filter.column) != null ?
                typeof(T).GetProperty(filter.column).PropertyType :
                typeof(T).GetField(filter.column).FieldType;
            var rp = Expression.PropertyOrField(r, filter.column); // rp is a parameter expression "r.property"
            dynamic values = typeof(DbUtilityHelper).GetMethod("MakeListOfSpecificType").MakeGenericMethod(columnType).Invoke(null, new object[] { filter.values });
            var filterValues = Expression.Constant(values, typeof(IEnumerable<>).MakeGenericType(columnType));
            var value = Expression.Parameter(columnType, "value");

            Expression methodCallExpression = null;
            switch (filter.operation)
            {
                case FilterOp.Equals:
                    methodCallExpression = Expression.Equal(rp, value);
                    break;
                case FilterOp.GreaterThan:
                    methodCallExpression = Expression.GreaterThan(rp, value);
                    break;
                case FilterOp.GreaterThanEquals:
                    methodCallExpression = Expression.GreaterThanOrEqual(rp, value);
                    break;
                case FilterOp.LessThan:
                    methodCallExpression = Expression.LessThan(rp, value);
                    break;
                case FilterOp.LessThanEquals:
                    methodCallExpression = Expression.LessThanOrEqual(rp, value);
                    break;
                case FilterOp.Contains:
                    if (columnType != typeof(string))
                    {
                        throw new WebSharedException("Unable to perform contains on non-string field/property.");
                    }
                    var containsMethod = columnType.GetMethod("Contains", new[] { columnType });
                    methodCallExpression = Expression.Call(rp, containsMethod, value);
                    break;
                default:
                    throw new WebSharedException("Unknown operation - " + filter.operation);
            }

            var equalLbd = Expression.Lambda(methodCallExpression, value); // value => methodCallExpression (r.property, value)
            var any = Expression.Call(typeof(Enumerable), "Any", new Type[] { columnType }, filterValues, equalLbd); // filter.values.Any( value => methodCallExpression (r.property, value))
            var exp = Expression.Lambda(any, r); // r => filter.values.Any( value => methodCallExpression (r.property, value))

            return (Expression<Func<T, bool>>)exp;
        }

    }
}
