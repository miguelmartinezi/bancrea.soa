﻿using Bancrea.ESB.Shared.Exceptions;
using Bancrea.ESB.Shared.Models;
using Bancrea.ESB.Shared.Utilities;
using Bancrea.ESB.Shared.Utilities.DbFilters;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Bancrea.ESB.Shared.Utilities
{
    /// <summary>
    /// This class contains the common utility methods for the DbSet.
    /// </summary>
    public class DbUtility
    {
        /// <summary>
        /// Add pagination and execute the query.
        /// 
        /// The result set will be paginated with the specified page size and only the page of the specified index will be returned.
        /// </summary>
        /// 
        /// <param name="query">The db query</param>
        /// <param name="currentPage">The page to be returned. This is a 1-based index.</param>
        /// <param name="pageSize">The page size, if -1, show all item in a single page </param>
        /// <returns>The data set of the specified page.</returns>
        public static PagedData<T> Paginate<T>(IQueryable<T> query, int currentPage, int pageSize)
        {
            if (pageSize == -1)
            {
                return new PagedData<T>(query, 1, 1);
            }

            if (pageSize == 0)
            {
                return new PagedData<T>(query.Take(0).ToArray(), 0, 0);
            }

            var totalPages = (int)Math.Ceiling((decimal)query.Count() / pageSize);

            // Adjust for zero-based index.
            currentPage = (currentPage <= 0) ? 0 : currentPage - 1;

            return new PagedData<T>(query
                .Skip(currentPage * pageSize)
                .Take(pageSize)
                .ToList(), currentPage + 1, totalPages);
        }

        /// <summary>
        /// Add sorting to the provided query based on the specified sort column and sort order.        
        /// </summary>
        /// 
        /// <param name="query">The db query</param>
        /// <param name="sortColumn">The column to be sorted.</param>
        /// <param name="sortOrder">The order of the sorting. @see SortOrder</param>        
        /// <returns>The query with the sorting added.</returns>
        /// 
        public static IQueryable<T> SortQuery<T>(IQueryable<T> query, string sortColumn, string sortOrder)
        {
            return SortQuery<T>(query, sortColumn, ParseSortOrder(sortOrder));
        }

        /// <summary>
        /// Add sorting to the provided query based on the specified sort column and sort order.        
        /// </summary>
        /// 
        /// <param name="query">The db query</param>
        /// <param name="sortColumn">The column to be sorted.</param>
        /// <param name="sortOrder">The order of the sorting. @see SortOrder</param>        
        /// <returns>The query with the sorting added.</returns>
        /// 
        public static IQueryable<T> SortQuery<T>(IQueryable<T> query, string sortColumn, SortOrder sortOrder)
        {
            var param = Expression.Parameter(typeof(T), "row");
            var body = Expression.PropertyOrField(param, sortColumn);

            Type columnType = body.Type;
            var orderBy = Expression.Lambda(body, param);

            //query.OrderBy((Expression<Func<T,bool>>) orderBy);
            IEnumerable<MethodInfo> ascAndDescMethods = typeof(Queryable).GetMethods().Where(m =>
            {
                var parameters = m.GetParameters();
                return (string.Equals(m.Name, "OrderByDescending") || string.Equals(m.Name, "OrderBy")) &&
                    parameters.Length == 2 &&
                    parameters[0].ParameterType.GetGenericTypeDefinition() == typeof(IQueryable<>) &&
                    parameters[1].ParameterType.GetGenericTypeDefinition() == typeof(Expression<>);
            });
            if (sortOrder == SortOrder.DESC)
            {
                var orderByDescMethod = ascAndDescMethods.First(m => string.Equals(m.Name, "OrderByDescending"));
                return (IQueryable<T>)orderByDescMethod.MakeGenericMethod(typeof(T), columnType).Invoke(null, new object[] { query, orderBy });
            }
            else
            {
                var orderByAscMethod = ascAndDescMethods.First<MethodInfo>(m => string.Equals(m.Name, "OrderBy"));
                return (IQueryable<T>)orderByAscMethod.MakeGenericMethod(typeof(T), columnType).Invoke(null, new object[] { query, orderBy });
            }
        }

        /// <summary>
        /// Parse the sort order value.
        /// </summary>
        /// <param name="value">The string value of the sort order.</param>
        /// <returns>Returns the corresponding enum. Note that if the value is empty or null, it will return the default sort order - ASC.</returns>
        public static SortOrder ParseSortOrder(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return SortOrder.ASC;
            }
            else
            {
                return (SortOrder)Enum.Parse(typeof(SortOrder), value, true);
            }
        }

        /// <summary>
        /// For each filter in the filter list, this method finds the corresponding IFilterExpGenerator class based on the filter.operation property
        /// and apply the condition generated by the IFilterExpGenerator class.
        /// </summary>
        /// <param name="query">The oringinal query</param>
        /// <param name="query">A list of DbFilter that needs to apply</param>
        /// <returns>Returns the filtered query</returns>
        public static IQueryable<T> ApplyFilter<T>(IQueryable<T> query, IEnumerable<DbFilter> filters)
        {
            var generatorTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(type => typeof(IFilterExpressionGenerator).IsAssignableFrom(type) && !type.IsInterface);
            var expGenerators = generatorTypes.Select(type => Activator.CreateInstance(type)).Cast<IFilterExpressionGenerator>().ToList();

            foreach (var filter in filters)
            {
                var suitedGenerators = expGenerators.Where(g => g.OperationsApplied.Contains(filter.operation));
                if (suitedGenerators.Count() == 0)
                {
                    throw new WebSharedException("Comparison Expression Generator not implemented for the operation: " + filter.operation);
                }
                if (suitedGenerators.Count() > 1)
                {
                    throw new WebSharedException("Multiple Comparison Expression Generators found!");
                }
                var generator = suitedGenerators.First();
                var condition = generator.GenerateFilterExp<T>(filter);
                query = query.Where(condition);
            }

            return query;
        }

        /// <summary>
        /// Return the entities from the specified IDbSet based on the provided query parameters.
        ///         
        /// </summary>
        /// <typeparam name="T">The type that defines the db set.</typeparam>
        /// <param name="dbSet">The dbset</param>
        /// <param name="parameters">The query parameters</param>
        /// <returns></returns>
        public static PagedData<T> GetEntities<T>(IDbSet<T> dbSet, IQueryParameters parameters) where T : class
        {
            try
            {
                var query = from x in dbSet
                            select x;

                if (parameters.Filters != null)
                {
                    query = DbUtility.ApplyFilter<T>(query, parameters.Filters);
                }

                var sortColumn = (string.IsNullOrWhiteSpace(parameters.SortColumn)) ? typeof(T).GetProperties().First().Name : parameters.SortColumn;

                query = DbUtility.SortQuery<T>(query, sortColumn, DbUtility.ParseSortOrder(parameters.SortOrder));

                return DbUtility.Paginate<T>(query, parameters.CurrentPage, parameters.PageSize);
            }
            catch (Exception e)
            {
                throw new WebSharedException("Unable to retrieve the entities.", e);
            }
        }

        public enum SortOrder
        {
            ASC,
            DESC
        }
    }

    public interface IPageParameters
    {
        string SortColumn { get; set; }
        string SortOrder { get; set; }
        int CurrentPage { get; set; }
        int PageSize { get; set; }
    }

    public interface IFilterParameters
    {
        List<DbFilter> Filters { get; set; }
    }

    public interface IQueryParameters : IPageParameters, IFilterParameters
    {
    }

    public class PageParameters : IPageParameters
    {
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

        public PageParameters()
        {
            PageSize = 9999;
            CurrentPage = 1;
        }
    }

    public class QueryParameters : PageParameters, IQueryParameters
    {
        public List<DbFilter> Filters { get; set; }
    }
}
