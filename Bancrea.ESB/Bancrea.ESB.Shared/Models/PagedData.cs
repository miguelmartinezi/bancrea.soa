﻿using System.Collections.Generic;

namespace Bancrea.ESB.Shared.Models
{
    public class PagedData<T>
    {
        public List<T> Data;
        public int CurrentPage;
        public int TotalPages;

        public PagedData()
        {
            Data = new List<T>();
            CurrentPage = -1;
            TotalPages = -1;
        }

        public PagedData(dynamic data, int currentPage, int totalPages)
        {
            this.Data = data;
            this.CurrentPage = currentPage;
            this.TotalPages = totalPages;
        }
    }
}
